# POSeidon

POSeidon (POStrocessing enhanced by interoperable domains for offline number-crunching) is a POSt processing collection 
of Python tools for "offline" computation of quantities of interest from heavy data generated
by large scale simulations (e.g. lUQness or SPhinx).

## Dependencies

You will need `Python` (>= 3), with `numpy`,  `matplotlib` (for plotting), `h5py`(for hdf5 I/O),
and Qt for GUI based stuff.

## How to use

Loading the data can be done as follows:
    
      import base.data.Data as Data
      
      # Load data
      dt = Data("<filename[.xdmf][.bin][.vts]>")
      
      # Compute norm
      nrm = dt.norm(2)
      
      dt.plot<J("<name>")
  
## Structure

List of folders with usage:

- base/ : contains all basic objects, Data, Histogram, Mean and Variance manipulation
- examples/ : a few examples of the usage of the scripts
- gui/ : gui based loading and manipulations
- offline_post/ : utilities for offline post processing
- profiling/ : stuff for profiling and plotting of profiled code