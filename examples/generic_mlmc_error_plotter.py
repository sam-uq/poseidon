#! /usr/bin/env python

import data

import fnmatch
import os
import sys

import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm 
#sys.path.append(os.environ['PETSC_DIR'] + "/bin/pythonscripts/")

#from PetscBinaryIO import *
##times = [0, 1,3, 4, 5]
##times = [4]
errOffs = [18.]
errO = [-1.]
wtimeO = [-1./5., -1./3.]
Ocolor = ['k--', 'k:', 'k-.']
ref_base_name = None
N = None
R = None
coll_ranges = None
wtime_table = None

# 20 KL MC euler etc...
#folder = "20kl"
#times = [2]
#repeats = 10
#tp = "mean"
#comp = "w"
#coll_sizes = [16, 32, 64, 128, 256, 512]
#N = len(coll_sizes)
#R = range(N)
#coll_ranges = [slice(N-1), slice(1,N)]
#ref_size = 1024
#ref_base_name = "mlmc-01-eul"
#names = ["mc-01-eul", "mlmc-01-eul"]
#names_human = [r"$SLMC$", r"$MLMC$"]
#ref_batch = 9
#wtime_table = [
#[0.21, 3.3, 142.02, 10477.48, 1460579.0],  # MC
#[2.00, 17.95, 162.00, 1180.99, 19801.61]  # MLMC
#]

#2 KL gauss euler etc...
folder = "2kl"
times = [2]
repeats = 1
tp = "mean"
comp = "w"
coll_sizes = [64, 256]
ref_size = 512
names = ["gauss01-eul"]
names_human = [r"$\nu = 0$"]

if not ref_base_name:
    set_base_name = False
else:
    set_base_name = True
if N == None:
    N = len(coll_sizes)
if R == None:
    R = range(N)
if coll_ranges == None:
    coll_ranges = [slice(N) for i in range(len(names))]

for t in times:
    abserr = []
    relerr = []
    stderr = []

    for (j, name) in enumerate(names):
        if not set_base_name:
            ref_base_name = name
            
        if repeats != 1:
            ref_name = "{}/{}-{}_{},{},1_{}_{}_f{}.bin".format(folder, ref_base_name, ref_batch, ref_size, ref_size, comp, tp, t)
        else:
            ref_name = "{}/{}_{},{},1_{}_{}_f{}.bin".format(folder, ref_base_name, ref_size, ref_size, comp, tp, t)
        ref = data.Data(ref_name, refactor=True)
        ref_norm = ref.norm(2)
        print("Reference norm: {}".format(ref_norm))

        abserr.append([])
        relerr.append([])
        stderr.append([])
        a = abserr[-1]
        b = relerr[-1]
        c = stderr[-1]

        for i in R[coll_ranges[j]]:
            
            n = 0
            errMean = 0.0
            errM2 = 0
            
            for batch in range(repeats):
                coll_size = coll_sizes[i]
                if repeats != 1:
                    coll_name = "{}/{}-{}_{},{},1_{}_{}_f{}.bin".format(folder, name, batch, coll_size, coll_size, comp, tp, t)
                else:
                    coll_name = "{}/{}_{},{},1_{}_{}_f{}.bin".format(folder, name, coll_size, coll_size, comp, tp, t)

                ratio = ref_size / coll_size
                
                dt = data.Data(coll_name, refactor=True)
                
                err = ref - dt
                
                n += 1
                errNorm = err.norm(2)[0]
                
                delta = errNorm - errMean
                errMean += delta / float(n)
                errM2 += delta * (errNorm - errMean)
            
            if n < 2:
                errVar = 0
            else:
                errVar = errM2 / (float(n) - 1.)
                
            errStd = np.sqrt(errVar)
            
            a.append(errMean)
            b.append(errMean / ref_norm[0])
            c.append(errStd)
            print("Error at {}: abs = {}, rel = {}, std = {}.".format(coll_size, a[-1], a[-1] / ref_norm[0], c[-1]))
        

    print("Absolute error.")
    print(abserr)
    print("Relative error.")
    print(relerr)
    #relerr = np.array(relerr)
    #abserr = np.array(abserr)

    plt.figure()

    num_plots = len(names)
    colormap = plt.cm.gist_ncar
    plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, 0.9, num_plots)])

    plt.gca().set_xscale("log", nonposx='clip')
    plt.gca().set_yscale("log", nonposy='clip')

    for i in range(0, len(relerr)):
        #plt.loglog(coll_sizes[coll_ranges[i]], relerr[i])
        if repeats > 1:
            plt.errorbar(coll_sizes[coll_ranges[i]], relerr[i], yerr = stderr[i], label=names_human[i])
        else:
            plt.plot(coll_sizes[coll_ranges[i]], relerr[i], label=names_human[i])
    plt.plot(coll_sizes[coll_ranges[-1]], errOffs[-1]*np.array(coll_sizes[coll_ranges[-1]])**errO[-1], Ocolor[-1], label="$O(N^{{ {0} }})$".format(errO[-1]))
    plt.grid(True)
    plt.title(r"Relative $L^2$ error with different viscosities VS mesh size (s = {})".format(t))
    plt.xlabel('N')
    plt.ylabel(r'$|| \eta - \eta_{ex} ||_{L^2} / || \eta_{ex} ||_{L^2}$')
    plt.legend()
    plt.savefig("{}_rel_error_t{}.pdf".format(folder, t))
    plt.show(block=False)
    
    if wtime_table != None:
        plt.figure()

        num_plots = len(names)
        colormap = plt.cm.gist_ncar
        plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, 0.9, num_plots)])

        plt.gca().set_xscale("log", nonposx='clip')
        plt.gca().set_yscale("log", nonposy='clip')

        for i in range(0, len(relerr)):
            #plt.loglog(coll_sizes[coll_ranges[i]], relerr[i])
            if repeats > 1:
                plt.errorbar(wtime_table[i], relerr[i], yerr = stderr[i], label=names_human[i])
            else:
                plt.plot(wtime_table[i], relerr[i], label=names_human[i])
            plt.plot(wtime_table[i], np.array(wtime_table[i])**wtimeO[i], Ocolor[i], label=r"$O(N^{{ {0:.2f} }})$".format(wtimeO[i]))
        plt.grid(True)
        plt.title(r"Relative $L^2$ error with different viscosities VS (avg.) wall time (s = {})".format(t))
        plt.xlabel('Wall time [s]')
        plt.ylabel(r'$|| \eta - \eta_{ex} ||_{L^2} / || \eta_{ex} ||_{L^2}$')
        plt.legend()
        plt.savefig("{}_rel_error_wtime_t{}.pdf".format(folder, t))
        plt.show(block=False)

plt.show(block=False)
#plt.show(block=False)
