#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Utility to get histograms from multiple data.

Description.

author: Filippo Leonardi
website: filippoleonardi.ch
last edited: January 2014
"""

import fnmatch
import os

import sys
sys.path.append("../")

from tools import *
import data
import drill

dr = drill.Drill([(0.25,0.5),(0.25,0.75)])

pattern = "*.bin"

for f in os.listdir("./"):
  if fnmatch.fnmatch(f, pattern):
    print(f)
    dr.push_data(data.Data(f, refactor = True, preload = True))
  
dr.report()
dr.plot()
