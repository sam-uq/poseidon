#! /usr/bin/env python

import re

import matplotlib.pyplot as plt
import pylab
import numpy as np
import collections

class ProfilerParser():
    """
    Parse a luqness style prfiler, written in Python format.
    """
    width = 3
    space = 1

    phases = ['integrate_begin', 'solve_post_begin', 'solve_post_end',
              'level_post_begin', 'integrate_end', 'profiler_destructor']
    phases_names = ['Solver', 'Post solve', 'Wait intra lvl.', 'Post level', 'Wait inter level', 'Destructor']

    def parse(self, alloc_parser, fileslist):
        # For each key contains a list of numbers for each process
        self.alloc_parser = alloc_parser
        self.profiling_data = collections.OrderedDict()

        for file in fileslist:
            with open(file) as f:
                lines = 0
                for line in f:
                    lines += 1
                    if line[0] == "#":
                        pass
                    else:
                        match = re.search("timings_rk([0-9]+) = {", line)
                        match2 = re.search("}", line)
                        match3 = re.search("\"(.*)\" : (\d+.\d+),?", line)
                        if match:
                            try:
                                stage = 0
                                rid = int(match.group(1))
                                print("Inside rank data {}".format(rid))
                            except ValueError:
                                print("Cannot parse rank number...")
                        elif match2:
                            stage = -1
                            last_rid = rid
                            rid = -1
                        elif match3 and rid >= 0:
                            #print("Matching 3 {} and {}".format(match3.group(1), match3.group(2)))
                            try:
                                identifier = match3.group(1)

                                timing = float(match3.group(2))
                            except:
                                print("Unable to match data.")
                            if identifier in self.phases:
                                try:
                                    self.profiling_data[identifier]
                                except KeyError:
                                    print("Adding key {}".format(identifier))
                                    self.profiling_data[identifier] = np.zeros(alloc_parser.ranks())

                                self.profiling_data[identifier][rid] = timing

                            stage += 1
                try:
                    timing = self.profiling_data["profiler_destructor"][last_rid]
                except:
                    try:
                        timing = self.profiling_data["integrate_end"][last_rid]
                    except:
                        timing = -1
                print("Rk {0}: Read {1} lines from file {2}, time used by this rank {3}".format(last_rid, lines, file,
                      timing))

    def plot(self, name_or_fig):

        self.num_levels = self.alloc_parser.num_levels

        xcoords = []
        spacing = 0.
        rank_line = []
        deepness_ticks = [self.width / 2.]
        for rk in self.alloc_parser.rk_list:
            if rk.rid > 0 and rk.intra_dom_col != prev_rk.intra_dom_col:
                spacing += self.space
                deepness_ticks.append(self.width * rk.rid + spacing + self.width / 2)
            xcoords.append(self.width * rk.rid + spacing)
            if rk.rid % self.alloc_parser.node_size() == 0:
                rank_line.append(xcoords[rk.rid])
            prev_rk = rk

        num_stacks = len(self.profiling_data)
        color_idx = np.linspace(0, 1, num_stacks)
        #print(color_idx)
        self.color = {stage : plt.cm.cool(i) for (stage, i) in zip(self.phases, color_idx)}

        #plt.clf()

        fig = plt.figure()
        self.ax = plt.subplot(111)
        #ax.get_xticklabels().set_fontsize(20)

        self.M = 0
        n_old = 0

        for phase, phase_name in zip(self.phases, self.phases_names):
            try:
                n = np.array(self.profiling_data[phase]) - n_old
                self.ax.bar(xcoords, n, 3, color=self.color[phase], bottom=n_old, label=phase_name)
                n_old = np.array(self.profiling_data[phase])

                self.M = max(max(n), self.M)
            except:
                print("Invalid phase?")

        for r in rank_line:
            plt.plot([r,r], [0,self.M*1.1], 'r--')


        self.ax.set_ylim([-0.1*self.M,self.M*1.1])

        self.ax2 = self.ax.twinx()
        self.ax2.set_ylim([0,self.num_levels*12])

        self.ax2.bar(xcoords, self.alloc_parser.generate_human_levels_list(), 3, color="red", bottom=0, label="lvl.")

        deepness = self.alloc_parser.generate_human_deepness_list()

        self.ax2.set_xticks(deepness_ticks)
        self.ax2.set_xticklabels( deepness, size=6 )
        self.ax2.set_yticks(range(1,self.num_levels))
        self.ax2.set_yticklabels(range(1,self.num_levels), size=6)

        print(len(self.ax.get_xticklabels( )))

        for t, r in zip( self.ax.get_xticklabels(), [rk.rid for rk in self.alloc_parser.rk_list] ):
            if deepness[r][0] == 'R' or  deepness[r][0] == 'r' or  deepness[r][0] == 's':
                y = 0
            else:
                y = -0.05
            t.set_y( y )

        # Shrink current axis by 20%
        box = self.ax.get_position()
        self.ax.set_position([box.x0, box.y0, box.width * 0.9, box.height])
        self.ax2.set_position([box.x0, box.y0, box.width * 0.9, box.height])
        # Put a legend to the right of the current axis

        self.ax.legend(loc='center left', bbox_to_anchor=(1, 0.5),prop={'size':6})

        self.ax.xaxis.label.set_fontsize(7)
        self.ax2.xaxis.label.set_fontsize(7)
        #ax.get_xticklabels().fontsize = 20
        #ax.get_xticklabels().set_fontsize(20)

        #plt.legend()
        plt.title("Profiling results")
        plt.show()
        #plt.savefig(name)

    def rescale(self):
        #self.ax.set_xlim([0,self.self.num_levels*12])
        self.ax.set_ylim([-0.1*self.M,self.M*1.1])
        self.ax2.set_ylim([0,self.num_levels*12])
