#! /usr/bin/env python

import sys
import re

import matplotlib.pyplot as plt
import pylab
import numpy as np

import collections
import xml.etree.ElementTree as ET

MAX_DEPTH = 15

class Stage:
    def __init__(self, parent = None, name = None, elapsed = None, count = None):
        self.name = name
        self.elapsed = elapsed
        self.parent = parent
        self.perc = None
        if parent is None:
            self.depth = 0
        else:
            self.depth = parent.depth + 1
        self.count = count
        self.nadds = None
        self.stages = {}

    def add_substage(self, stage):
        self.stages[stage.name] = stage

    def read_xml(self, xml_reader):
        self.name = xml_reader.attrib["name"]
        self.count = int(xml_reader.attrib["count"])
        print("*{3}> Read substage '{0}', elapsed {1}, count {2}".format(self.name, self.elapsed, self.count, self.depth * "-"))
        for child in xml_reader:
            if child.tag == "elapsed":
                self.elapsed = float(child.text)
            elif child.tag == "Stage":
                child_stage = Stage(self)
                child_stage.read_xml(child)
                self.add_substage(child_stage)

    def add_stage(self, other):
        if self.nadds is None:
            self.nadds = 1

        self.count += other.count
        self.elapsed += other.elapsed
        print("Adding stages substage '{0}'".format(self.name))
        for other_child_name, other_child in other.stages.items():
            if other_child_name in self.stages:
                self.stages[other_child_name].add_stage(other_child)
            else:
                print("Stage '{0}' not present, creating new one...".format(other_child.name))
                self.stages[other_child_name] = other_child

        self.nadds += 1

    def finalize(self):
        self.elapsed /= self.nadds
        self.count //= self.nadds
        if not self.parent is None:
            self.perc = self.elapsed / self.parent.elapsed
        for other_child_name, other_child in self.stages.items():
            other_child.finalize()

    def compute(self):
        pass

    def display(self):
        if self.perc is None:
            print("*{3}>{4} {0:25s} {1:10.2f} {2:10d}".format(self.name, self.elapsed, self.count, self.depth * "-", (MAX_DEPTH - self.depth) * " "))
        else:
            print("*{3}>{4} {0:25s} {1:10.2f} {2:10d} {5:15.2f}".format(self.name, self.elapsed, self.count, self.depth * "-", (MAX_DEPTH - self.depth) * " ", self.perc * 100))
        for other_child_name, other_child in self.stages.items():
            other_child.display()


class ProfilerXmlParser:
    """
    Helps visualization of Xml profiler for sphinx.
    """

    def __init__(self, filename):
        tree = ET.parse(filename)
        root = tree.getroot()

        print("Reading '{0}'...".format(filename))
        self.tree = Stage()
        self.tree.read_xml(root)

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Need at least one argument...")
        exit()

    fname = sys.argv[1]
    if len(sys.argv) >= 3:
        nranks = int(sys.argv[2])
    else:
        nranks = 10000

    parser = []

    for i in range(0, nranks):
        try:
            parser.append(ProfilerXmlParser(fname + "_rk" + str(i) + ".xml"))
        except FileNotFoundError:
            break

    for i, new_parser in enumerate(parser):
        if i >= 1:
            parser[0].tree.add_stage(new_parser.tree)

    parser[0].tree.finalize()
    parser[0].tree.display()
