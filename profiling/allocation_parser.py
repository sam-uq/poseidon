#! /usr/bin/env python

import sys

#import prof

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

def rectangularize(a):
    #print(int(np.floor(np.sqrt(a))))
    for b in range(1,int(np.floor(np.sqrt(a)))+1):
        #print(b)
        if divmod(a, b)[1] == 0:
            #print("c = {}".format(b))
            c = b
    return int(c), int(a) // int(c)

def square(a):
    b = np.ceil(np.sqrt(a))
    (c,d) = divmod(a, b)
    return int(b), int(c), int(d)

def compute_size(shape, size, spacing):
    """

    """
    #print(size + 1)
    #print(shape)
    #print(spacing)
    return shape * size + (shape + 1) * spacing

class PlotConfig:
    """

    """
    sim_shape = np.array([0.1,0.1])
    sim_spacing = sim_shape / 2.
    subdom_spacing = sim_shape / 4.
    dom_spacing = sim_shape / 2. + 0.1
    lvl_spacing = sim_shape / 2. + 0.3
    arrow_length = 0.2
    arrow_width = 0.3
    lvl_color = "grey"
    root_color = "orange"
    super_root_color = "red"
    smallest_root_color = "yellow"
    color_map = plt.cm.rainbow # plt.cm.cool
    sim_color = "black"

class Simulation:
    @staticmethod
    def place(ax, coord, color = "black", weight = None):

        if weight == None:
            shape = PlotConfig.sim_shape.copy()
        else:
            shape = PlotConfig.sim_shape * np.log10(weight)


        ax.add_patch(Rectangle(coord, shape[0], shape[1], facecolor=color, antialiased=False, alpha = .8))

        return shape

class Rank:
    def __init__(self, rid, lvl, intra_dom_col, intra_lvl_col, inter_lvl_col, sm_id_start, sim_num, deepness, sim_count="real"):
        self.rid = rid
        self.lvl = lvl
        self.intra_dom_col = intra_dom_col
        self.intra_lvl_col = intra_lvl_col
        self.inter_lvl_col = inter_lvl_col
        self.sm_id_start = sm_id_start
        self.sim_num = sim_num
        if sim_count == "real":
            self.fake_sim_num = self.sim_num
        elif sim_count == "log":
            self.fake_sim_num = int(np.floor(np.log10(self.sim_num)) + 1)
        elif sim_count == "const":
            self.fake_sim_num = 1
        else:
            try:
                self.fake_sim_num = int(sim_count)
            except:
                self.fake_sim_num = self.sim_num

        self.root = False
        self.super_root = False
        self.smallest_root = False
        self.deepness = 0
        if deepness == "-":
            self.root = True
            self.super_root = True
        elif deepness == "|":
            self.root = True
            self.smallest_root = True
        elif deepness == "^":
            self.root = True
        else:
            try:
                self.deepness = int(deepness)
            except ValueError:
                print("Invalid deepness {}".format(deepness))
        if self.root and self.inter_lvl_col < 0:
            print("Root should have a positive inter level color.")
        elif not self.root and self.inter_lvl_col >= 0:
            print("Non Root should not have a positive inter level color.")


    def set_node(self, node):
        self.node = node

    def human_deepness(self):
        if self.super_root:
            return "R{}".format(self.lvl)
        elif self.smallest_root:
            return "S{}".format(self.lvl)
        elif self.root:
            return "r{}".format(self.lvl)
        else:
            return str(self.deepness)

    def print(self):
        print("Rank {} with dom {} a lvl. {}.".format(self.rid, self.intra_dom_col, self.lvl))

    def place(self, ax, coord):

        ax.add_patch(Rectangle(coord, PlotConfig.sim_shape[0], PlotConfig.sim_shape[1], facecolor="black", zorder = 10))


class Domain:
    def __init__(self, intra_dom_col):
        self.intra_dom_col = intra_dom_col

        self.rk_list = []

        self.sim_num = None
        self.fake_sim_num = None

        self.num_nodes = -1

    def ranks(self):
        return len(self.rk_list)

    def add_rank(self, rk):
        if rk.intra_dom_col != self.intra_dom_col or (self.sim_num != None and self.sim_num != rk.sim_num):
            print("Rank is not compatible with Domain {} != {} or {} != {}".format(rk.intra_dom_col, self.intra_dom_col, self.sim_num, rk.sim_num))
        else:
            self.rk_list.append(rk)
            self.sim_num = rk.sim_num
            self.fake_sim_num = rk.fake_sim_num
            self.lvl = rk.lvl
            self.root = rk.root
            self.super_root = rk.super_root
            self.smallest_root = rk.smallest_root
            try:
                self.weight = rk.weight
            except:
                pass

    def set_num_nodes(self, num_nodes):
        self.num_nodes = num_nodes

    def print(self):
        l = ";"
        for rk in self.rk_list:
            l += str(rk.rid) + ";"
        print("Domain {} with {} sim. num. has ranks {}.".format(self.intra_dom_col, self.sim_num, l))

    def place(self, ax, coord, root_coord=None, weighted_sim = False):
        #size = np.array([0.1,0.1])
        #ax.add_patch(Rectangle(coord - size / 2, size[0], size[1], facecolor="grey"))
        r, c, o = square(self.fake_sim_num)
        #size = compute_size(np.array([r,r]), PlotConfig.sim_shape, PlotConfig.sim_spacing)
        #print(r, size)
        print(self.fake_sim_num, r)

        #size_out = size + 2*PlotConfig.subdom_spacing


        bcolor = "black"
        if self.rk_list[0].root == True:
            bcolor = PlotConfig.root_color
        if self.rk_list[0].super_root == True:
            bcolor = PlotConfig.super_root_color
        if self.rk_list[0].smallest_root == True:
            bcolor = PlotConfig.smallest_root_color

        wght = None
        if weighted_sim:
            try:
                wght = self.weight
            except:
                #print("asdasda")
                pass

        idx = 0
        #start_coord = coord - size / 2 + PlotConfig.sim_spacing
        start_coord = coord + PlotConfig.sim_spacing
        sim_coord = start_coord.copy()
        #print("---- {} / {}".format(r,c))
        for idx in range(self.fake_sim_num):
            coord_idx = np.array([(idx+1) % r, (idx+1) // r])
            #print(coord)
            #print(sim_coord)
            shape = Simulation.place(ax, sim_coord, PlotConfig.sim_color, weight = wght)
            sim_coord = start_coord + (shape + PlotConfig.sim_spacing) * coord_idx
            #print(coord_idx, sim_coord)
            idx += 1

        space_needed = PlotConfig.sim_spacing + (shape + PlotConfig.sim_spacing) * r
        #space_needed += PlotConfig.sim_spacing

        #ax.add_patch(Rectangle(coord - size / 2, size[0], size[1], facecolor="grey", zorder = -1))
        ax.add_patch(Rectangle(coord, space_needed[0], space_needed[1], facecolor=PlotConfig.lvl_color, zorder = -1, edgecolor=bcolor, linewidth=4, antialiased=False, fill=False))


        colors_idx = np.linspace(0, 1, self.num_nodes)
        edgecolor = [PlotConfig.color_map(X) for X in colors_idx]
        #print(self.num_nodes)
        color = ["black", "white"]
        C0 = 1
        #print(self.ranks())
        r, c = rectangularize(self.ranks())
        size2 = (space_needed - 2*PlotConfig.subdom_spacing) / np.array([r,c])
        #print(r,c)
        for j in range(c):
            if C0 == 1:
                C0 = 0
            else:
                C0 = 1
            C = C0
            for i, rk in zip(range(r), self.rk_list):
                if C == 1:
                    C = 0
                else:
                    C = 1
                #pass
                #print(size2)
                #ax.add_patch(Rectangle(coord - size / 2 + size2 * np.array([i,j]), size2[0], size2[1], facecolor="orange", zorder = 0, alpha=0.6))
                #print(rk.node)
                ax.add_patch(Rectangle(coord + PlotConfig.subdom_spacing + size2 * np.array([i,j]), size2[0], size2[1], facecolor=color[C], linestyle="dashed", zorder = 0, edgecolor=edgecolor[rk.node], alpha=.7, linewidth=3, antialiased=False))

        try:
            ax.annotate("",
                xy=(root_coord[0] + PlotConfig.dom_spacing[0], root_coord[1] + 2*PlotConfig.dom_spacing[1]), xycoords='data',
                xytext=(coord[0] , coord[1] + 2* PlotConfig.dom_spacing[1]), textcoords='data',
                size=10, va="center", ha="center",
                arrowprops=dict(arrowstyle="simple",
                                connectionstyle="arc3,rad=0.1", facecolor='white', edgecolor="black")
                )
        except:
            pass

        return space_needed


class Level:
    def __init__(self, lvl):
        self.lvl = lvl
        self.dom_list = []
        self.root = None

    def add_dom(self, dom):
        if self.lvl != dom.lvl:
            print("Level is not compatible with Domain {} != {}".format(self.lvl, dom.lvl))
        else:
            self.dom_list.append(dom)
            if dom.root:
                self.root = dom

    def place(self, ax, coord, last_coord = None, weighted_sim = False):
        lvl_patch_size = np.array([0.,0.])

        dom_coord = coord + PlotConfig.dom_spacing
        root_coord = None

        for dom in self.dom_list:
            size = dom.place(ax, dom_coord, root_coord, weighted_sim)
            root_coord = coord.copy()
            #root_coord =
            #print(size)
            lvl_patch_size[0] = max(lvl_patch_size[0], size[0] + PlotConfig.dom_spacing[0])
            lvl_patch_size[1] += size[1] + PlotConfig.dom_spacing[1]
            dom_coord[1] += size[1] + PlotConfig.dom_spacing[1]

        lvl_patch_size += PlotConfig.dom_spacing

        #print("---> {}".format(lvl_patch_size))
        ax.add_patch(Rectangle(coord, lvl_patch_size[0], lvl_patch_size[1], facecolor="gray", zorder = -2, alpha=1.0, hatch='\\'))

        try:
            #ax.arrow(last_coord[0], last_coord[1], PlotConfig.lvl_spacing[0] - PlotConfig.arrow_length, 0, head_width=PlotConfig.arrow_width, head_length=PlotConfig.arrow_length, fc='k', ec='k',
                            #connectionstyle="arc3")
            coord1 = coord.copy()
            coord2 = last_coord.copy()
            coord1[1] += 2*PlotConfig.dom_spacing[1]
            coord2[1] += 2*PlotConfig.dom_spacing[1]
            coord1[0] += PlotConfig.dom_spacing[0]
            coord2[0] -= PlotConfig.dom_spacing[0]

            ax.annotate("",
                xytext=(coord1[0], coord1[1]), xycoords='data',
                xy=(coord2[0], coord2[1]), textcoords='data',
                size=30, va="center", ha="center",
                arrowprops=dict(arrowstyle="simple",
                                connectionstyle="arc3,rad=-0.2", facecolor='white', edgecolor="black")
                )


            #bbox_props = dict(boxstyle="rarrow", fc=(0.8,0.9,0.9), ec="b", lw=2)
            #t = ax.text(0, 0, "Direction", ha="center", va="center", rotation=45,
            #size=15,
            #bbox=bbox_props)

            #bb = t.get_bbox_patch()
            #bb.set_boxstyle("rarrow", pad=0.6)
        except:
            pass

        return lvl_patch_size

class AllocatorParser:

    def __init__(self):
        pass

    def parse(self, file, sim_count = "real"):
        passed_distrline = False
        passed_levelline = False
        curr_node = 0

        self.rk_list = []
        self.dom_list = []

        #self.world_size = 0
        self.num_levels = 0
        self.intra_lvl_size = 0
        self.intra_dom_size = 0

        self.tot_nodes = 0

        self.lvlline = []

        print("Prasing {}...".format(file))
        with open(file) as f:
            for line in f:
                #print("Line {}".format(line))
                if line[0] == "/":
                    if line == "///////////  DISTRIBUTION OF SIMULATIONS  ///////////\n":
                        print("New section")
                        if passed_distrline == True:
                            print("Multiple distribution of simulations lines detected")
                            break
                        passed_distrline = True
                    elif line == "////////////////////  LEVEL DATA  ///////////////////\n":
                        print("New section")
                        if passed_levelline == True:
                            print("Multiple level lines detected")
                            break
                        passed_levelline = True
                    else:
                        pass
                elif passed_levelline and not passed_distrline:
                    list = line.split()
                    if len(list) != 14:
                        print("Useless line...")
                    else:
                        self.lvlline.append(list)
                        print(list)
                elif passed_distrline:
                    if line[0] == "#":
                        curr_node += 1
                        self.tot_nodes += 1
                    else:
                        list = line.split()
                        if list == []:
                            pass
                        elif list[0] == "rk.":
                            print("Header found")
                            pass
                        else:
                            try:
                                rid = int(list[0])
                                if rid != len(self.rk_list):
                                    print("Rank do not coincide, something fishy is happening...")
                                lvl = int(list[1])
                                if lvl + 1 >= self.num_levels:
                                    self.num_levels = lvl + 1
                                intra_dom_col = int(list[2])
                                if intra_dom_col + 1 >= self.intra_dom_size:
                                    self.intra_dom_size = intra_dom_col + 1
                                intra_lvl_col = int(list[3])
                                if intra_lvl_col + 1 >= self.intra_lvl_size:
                                    self.intra_lvl_size = intra_lvl_col + 1
                                inter_lvl_col = int(list[4])
                                sim_id_start = int(list[5])
                                sim_num = int(list[6])
                                deepness = list[7]
                                rk = Rank(rid, lvl, intra_dom_col, intra_lvl_col, inter_lvl_col, sim_id_start, sim_num, deepness, sim_count)
                                self.rk_list.append(rk)
                                rk.set_node(self.tot_nodes - 1)
                            except ValueError:
                                print("Invalid row?")
                        #print(list)


        print("File was read succesfully, got {} ranks on {} nodes and {} lvls, enjoy.".format(self.ranks(), self.tot_nodes, self.num_levels))

    def generate_data(self, weight_time = False):
        print("Generating domain.")

        self.dom_list = [Domain(intra_dom_col) for intra_dom_col in range(self.intra_dom_size)]
        self.lvl_list = [Level(lvl) for lvl in range(self.num_levels)]

        for rk in self.rk_list:
            self.dom_list[rk.intra_dom_col].add_rank(rk)

        for dom in self.dom_list:
            dom.set_num_nodes(self.tot_nodes)
            self.lvl_list[dom.lvl].add_dom(dom)

        for ll in self.lvlline:
            lvl = self.lvl_list[int(ll[0])]
            if lvl.lvl != int(ll[0]):
                print("Something went wrong")
            else:
                lvl.mesh = [int(x) for x in ll[1].split(",")]
                if weight_time:
                    wt = max(lvl.mesh[0], lvl.mesh[1], lvl.mesh[2])
                else:
                    wt = 1
                lvl.weight = lvl.mesh[0] * lvl.mesh[1] * lvl.mesh[2] * wt
                for dom in lvl.dom_list:
                    dom.weight = lvl.weight
                lvl.tot_sim = int(ll[2])

        #for dom in self.dom_list:
            #dom.print()

    def generate_human_deepness_list(self):
        ret = []
        prev_col = -1
        for rk in self.rk_list:
            if rk.intra_dom_col != prev_col:
                ret.append(rk.human_deepness())
                prev_col = rk.intra_dom_col
        return ret

    def generate_human_intra_dom_col_list(self):
        ret = []
        for rk in self.rk_list:
            ret.append(rk.intra_dom_col)

    def generate_human_levels_list(self):
        ret = []
        for rk in self.rk_list:
            ret.append(rk.lvl)
        return ret

    def ranks(self):
        return len(self.rk_list)

    def node_size(self):
        return self.ranks() / self.tot_nodes

    def plot(self, name_or_fig = None, weighted_sim = False):
        someX, someY = 2, 3

        if isinstance(name_or_fig, str):
            fig = plt.figure(figsize=(20, 20))
        else:
            fig = name_or_fig

        ax = fig.add_subplot(111, aspect='equal')
        coord = [0,0]
        #for rk in self.rk_list:
            #rk.place(ax, coord)
            #coord[0] += 0.2
        #for dom in self.dom_list:
            #size = dom.place(ax, coord)
            #coord[0] += size[0]
        coord += PlotConfig.lvl_spacing
        self.tot_size = PlotConfig.lvl_spacing.copy()
        last_coord = None
        for lvl in self.lvl_list:
            size = lvl.place(ax, coord, last_coord, weighted_sim)
            last_coord = coord + np.array([size[0], 0])
            coord[0] += size[0] + PlotConfig.lvl_spacing[0]
            self.tot_size[0] += size[0] + PlotConfig.lvl_spacing[0]
            self.tot_size[1] = max(self.tot_size[1], size[1] + PlotConfig.lvl_spacing[1])

        self.tot_size[1] += PlotConfig.lvl_spacing[1]

        #Create custom artists
        #simArtist = plt.Line2D((0,1),(0,0), color='k', marker='o', linestyle='b')
        anyArtist = [plt.Line2D((0,1),(0,0), color='k', linestyle='-', antialiased = False, linewidth=4)]
        #plt.quiverkey(QV1, 1.2, 0.515, 2, 'arrow 1', coordinates='data')
        #Create legend from custom artist/label lists
        ax.legend([plt.Line2D([0],[0.5], color='red', antialiased = False, linestyle='dashed', linewidth=4)] +
                anyArtist, ["asd", "asd2"], prop={'size':9})


        #plt.plot([0,0],[0,100])
        #plt.xlim([0,10])
        #plt.ylim([0,10])
        plt.xlim([0,self.tot_size[0]])
        plt.ylim([0,self.tot_size[1]])
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        if isinstance(name_or_fig, str):
            plt.savefig(name_or_fig, dpi=300)
        else:
            plt.show()

    def rescale(self):
        plt.xlim([0,self.tot_size[0]])
        plt.ylim([0,self.tot_size[1]])

if __name__ == "__main__":
    if len(sys.argv) > 1:
        file = sys.argv[1]
    if len(sys.argv) > 2:
        profile = sys.argv[2]
    a = AllocatorParser()
    a.parse(file, sim_count = "log")

    #print(a.generate_human_deepness_list())
    #print(a.generate_human_intra_dom_col_list())
    #print(a.generate_human_levels_list())

    #p = prof.ProfilerParser()
    #p.parse(a, profile)
    #p.plot("profiling.pdf")

    a.generate_data()

    a.plot("histo.pdf", weighted_sim = True)
    #a.plot("histo.png")
    #a.plot(weighted_sim = True)


