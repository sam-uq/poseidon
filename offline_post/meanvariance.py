#! /usr/bin/env python

import numpy as np

class MeanVariance:
  """
  
  """
  def __init__(self, novar = False, weights = None):
    """
    
    """
    
    if weights:
      self.use_weights = True
    else:
      self.use_weights = False
    self.novar = novar
      
    self.sumweight = 0
    self.nadds = 0
    
    self.mean = None
    self.m2 = None
    self.var = None
    self.finalized = False
    
  
  def push(self, data, weight = 1):
    """
    
    """
    
    self.nadds += 1
    if self.mean == None:
      if self.use_weights:
        self.mean = data
        self.sumweight = weight
        if not self.novar:
            try:
                self.m2 = np.zeros(data.shape)
            except AttributeError:
                self.m2 = 0
      else:
        self.mean = data
        if not self.novar:
            try:
                self.m2 = np.zeros(data.shape)
            except AttributeError:
                self.m2 = 0
    else:
      if self.use_weights:
        temp = weight + self.sumweight
        delta -= self.mean
        R = delta * (weight / temp)
        self.mean += R
        self.m2 += delta * (R * self.sumweight)
        self.sumweight = temp
      else:
        delta = data - self.mean
        self.mean = self.mean + delta / float(self.nadds)
        if not self.novar:
          self.m2 += delta * (data - self.mean)
        
    
    
  def finalize(self):
    """
    
    """
    self.finalized = True
    if not self.novar:
      if self.use_weights:
        self.var = self.m2 / self.sumweight * (self.nadds / (self.nadds - 1))
      else:
        self.var = self.m2 / self.nadds
    
  def mean(self):
    """
    
    """
    
    return self.mean
    
  def variance(self):
    """
    
    """
    
    if self.novar:
      return None
    if self.finalized:
      return self.var
    else:
      return self.m2 / self.nadds
    
  def save(self):
    """
    
    """
    pass
  
  def plot(self):
    """
    
    """
    pass
    
