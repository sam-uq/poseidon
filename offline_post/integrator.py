#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Utility to get histograms from multiple data.

Description.

author: Filippo Leonardi
website: filippoleonardi.ch
last edited: January 2014
"""

import fnmatch
import os

import sys
sys.path.append("../")

from tools import *
from meanvariance import *
import data
import numpy as np


w = [0.2955242247147528701738930, 0.2692667193099963550912269, 0.2190863625159820439955349, 0.1494513491505805931457763, 0.0666713443086881375935688]
temp = np.concatenate((w[::-1],w))
weights = np.kron(temp, temp) / 4

mav = MeanVariance(weights = True)

pattern = "2kl-gauss-better_1024_w_sim*_f0.bin"

i = 0
for f in os.listdir("./"):
  if fnmatch.fnmatch(f, pattern):
    print(f)
    mav.push(data.Data(f, refactor = True, preload = True),  weights[i])
    i += 1
  
mav.finalize()
np.save("mean", mav.mean.data)
np.save("variance", mav.variance().data)

