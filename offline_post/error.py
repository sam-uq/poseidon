#! /usr/bin/env python

import fnmatch
import os
import sys

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm 

sys.path.append(os.environ['PETSC_DIR'] + "/bin/pythonscripts/")

from PetscBinaryIO import *

ref_size = 1024

#coll_sizes = [32, 64, 128, 256, 512]
coll_sizes = [32, 64, 128, 256, 512]

n = np.zeros((len(coll_sizes),1))
mean_err = np.zeros((len(coll_sizes),1))
m2_err = np.zeros((len(coll_sizes),1))

max_batches = 10

for batch in range(0, max_batches):
  print("Error for batch {}...".format(batch))
  for i in range(0,len(coll_sizes)):
    coll_size = coll_sizes[i]
    coll_name = "{}/batch_{}_{}.npy".format(coll_size, batch, coll_size)
    ref_name = "{}/batch_{}_{}.npy".format(ref_size, batch, ref_size)
    coll_name = "{}_{}.npy".format(coll_size, batch, coll_size)
    ref_name = "{}_{}.npy".format(ref_size, batch, ref_size)

    ratio = ref_size / coll_size

    #ref, = PetscBinaryIO().readBinaryFile(ref_name)
    #ref = ref.reshape(ref_size, ref_size)
    ref = np.load(ref_name)


    print("Size {}...".format(coll_size))

    ref_norm = np.sqrt(np.sum(np.sum(np.abs(ref)  ** 2, 0), 0) / float(ref_size * ref_size))

    print("Norm of mean of ref {} is {}".format(ref_size, ref_norm))

    coll = np.load(coll_name)

    err = np.zeros([ref_size,ref_size])
    for of1 in range(0,int(ratio)):
      for of2 in range(0,int(ratio)):
        #print(of1, of2)
        err[of1::int(ratio), of2::int(ratio)] = ref[of1::int(ratio), of2::int(ratio)] - coll

    #plt.clf()
    #plt.pcolormesh(err)
    #plt.colorbar()
    #plt.savefig("batch_{}_{}_err".format(batch, coll_size))

    norm = np.sqrt(np.sum(np.sum(np.abs(err)  ** 2, 0), 0) / float(ref_size*ref_size))
    rel_norm = norm / ref_norm

    print("Norm: {}".format(norm))
    print("Rel Norm: {}".format(rel_norm))
    
    n[i] = n[i] + 1.
    delta = rel_norm - mean_err[i]
    mean_err[i] = mean_err[i] + delta/n[i]
    m2_err[i] = m2_err[i] + delta * (rel_norm - mean_err[i])
    
m2_err /= n

print(mean_err)
print(m2_err)