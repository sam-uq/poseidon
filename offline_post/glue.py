#! /usr/bin/env python

import fnmatch
import os
import sys

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm
from scipy import interpolate
import scipy

sys.path.append(os.environ['PETSC_DIR'] + "/bin/pythonscripts/")

from PetscBinaryIO import *

max_batches = 10

for batch in range(0, max_batches):

  #files = ["16_batch_{}_collect.npy".format(batch), "32_batch_{}_collect.npy".format(batch)]
  #sizes = [16, 32]
  #name = "32"

  files = ["16_batch_{}_collect.npy".format(batch), "32_batch_{}_collect.npy".format(batch),  "64_batch_{}_collect.npy".format(batch)]
  sizes = [16, 32, 64]
  name = "64"

  files = ["16_batch_{}_collect.npy".format(batch), "32_batch_{}_collect.npy".format(batch), "64_batch_{}_collect.npy".format(batch), "128_batch_{}_collect.npy".format(batch)]
  sizes = [16, 32, 64, 128]
  name = "128"

  #files = ["16_batch_{}_collect.npy".format(batch), "32_batch_{}_collect.npy".format(batch), "64_batch_{}_collect.npy".format(batch), "128_batch_{}_collect.npy".format(batch), "256_batch_{}_collect.npy".format(batch)]
  #sizes = [16, 32, 64, 128, 256]
  #name = "256"

  files = ["16_batch_{}_collect.npy".format(batch), "32_batch_{}_collect.npy".format(batch), "64_batch_{}_collect.npy".format(batch), "128_batch_{}_collect.npy".format(batch), "256_batch_{}_collect.npy".format(batch), "512_batch_{}_collect.npy".format(batch)]
  sizes = [16, 32, 64, 128, 256, 512]
  name = "512"

  files = ["16_batch_{}_collect.npy".format(batch), "32_batch_{}_collect.npy".format(batch), "64_batch_{}_collect.npy".format(batch), "128_batch_{}_collect.npy".format(batch), "256_batch_{}_collect.npy".format(batch), "512_batch_{}_collect.npy".format(batch), "1024_batch_{}_collect.npy".format(batch)]
  sizes = [16, 32, 64, 128, 256, 512, 1024]
  name = "1024"
  
  dire = name + "/"

  print("Loading {}...".format(files[0]))
  #f = files[0]
  #s  = sizes[0]
  #old_sum = np.load(f)
  ##old_sum = old_sum.reshape(s, s)
  #sum = np.zeros([sizes[1], sizes[1]])

  #for i in range(1,len(files)):
    #f = files[i]
    #s  = sizes[i]
    #print("Loading {}...".format(files[i]))
    #data = np.load(f)
    ##data = data.reshape(s, s)
    #print(data.shape)
    #sum = np.zeros([sizes[i], sizes[i]])
    #sum[0::2,0::2] = data[0::2,0::2] + old_sum
    #sum[1::2,0::2] = data[1::2,0::2] + old_sum
    #sum[0::2,1::2] = data[0::2,1::2] + old_sum
    #sum[1::2,1::2] = data[1::2,1::2] + old_sum

    #old_sum = sum

  #np.save("batch_{}_{}".format(batch, name), sum)
  #plt.clf()
  #plt.pcolormesh(sum)
  #plt.colorbar()
  #plt.savefig("batch_{}_{}.png".format(batch, name))


  f = dire + files[0]
  s  = sizes[0]
  old_sum = np.load(f)
  #old_sum = old_sum.reshape(s, s)
  sum = np.zeros([sizes[1], sizes[1]])

  sum = None

  s = 16

  for i in range(0,len(files)):
    f = dire + files[i]
    s_old = s
    s  = sizes[i]
    #data = np.load(f)
    #data = data.reshape(s, s)
    
    data = np.load(f)
    print(data.shape)
    
    if sum != None:
      x = np.linspace(2./s_old, 1 - 2./s_old, s_old)
      y = np.linspace(2./s_old, 1 - 2./s_old, s_old)
      print(sum.shape)
      print(x.shape)
      print(y.shape)
      f = scipy.interpolate.RectBivariateSpline(x, y, sum)
      #f = interpolate.interp2d(x, y, sum, kind='cubic')

      xnew = np.linspace(2./s, 1 - 2./s, s)
      ynew = np.linspace(2./s, 1 - 2./s, s)
      sum = f(xnew, ynew)
      
      sum += data
    else:
      sum = data

  np.save("{}_{}".format(name, batch), sum)
  plt.clf()
  plt.pcolormesh(sum)
  plt.colorbar()
  plt.savefig("{}_{}.png".format(name, batch))

  #----------------- interp

  #f = files[0]
  #s  = sizes[0]
  #old_sum = np.load(f)
  ##old_sum = old_sum.reshape(s, s)
  #sum = np.zeros([sizes[1], sizes[1]])

  #sum = None

  #s = 16

  #for i in range(0,len(files)):
    #f = files[i]
    #s_old = s
    #s  = sizes[i]
    ##data = np.load(f)
    ##data = data.reshape(s, s)
    
    #data = np.load(f)
    #print(data.shape)
    
    #if sum != None:
      #x = np.linspace(2./s_old, 1 - 2./s_old, s_old)
      #y = np.linspace(2./s_old, 1 - 2./s_old, s_old)
      #print(sum.shape)
      #print(x.shape)
      #print(y.shape)
      #f = interpolate.interp2d(x, y, sum, kind='cubic')

      #xnew = np.linspace(2./s, 1 - 2./s, s)
      #ynew = np.linspace(2./s, 1 - 2./s, s)
      #sum = f(xnew, ynew)
      
      #sum += data
    #else:
      #sum = data

  #np.save("{}".format(name), sum)
  #plt.clf()
  #plt.pcolormesh(sum)
  #plt.colorbar()
  #plt.savefig("{}.png".format(name))

  #N = 16
  #N2 = 512

  #f = "16_batch_0_collect.npy"

  #data = np.load(f)
  #x = np.linspace(2./N, 1 - 2./N, N)
  #y = np.linspace(2./N, 1 - 2./N, N)
  #f = interpolate.interp2d(x, y, data, kind='cubic')

  #xnew = np.linspace(2./N2, 1 - 2./N2, N2)
  #ynew = np.linspace(2./N2, 1 - 2./N2, N2)
  #znew = f(xnew, ynew)

  #plt.clf()
  #plt.pcolormesh(znew)
  #plt.colorbar()
  #plt.savefig("bicubic.png".format(name))