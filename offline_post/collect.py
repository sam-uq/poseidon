#! /usr/bin/env python

import fnmatch
import os
import sys

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

sys.path.append(os.environ['PETSC_DIR'] + "/bin/pythonscripts/")

from PetscBinaryIO import *

dof = 1
n = 0
batch = 0
batch_num = 4
batch_max = 10

#folder = "1024"
#dim_list = [1024, 512, 256, 128, 64, 32, 16]
#batch_list = [1, 4, 16, 64, 64, 64, 64]
#prefix_list = ["visc1", "visc1", "visc1", "visc1", "visc1", "visc1", "visc1"]
#name_list = ["diff", "diff", "diff", "diff", "diff", "diff", "mean"]

#folder = "512"
#dim_list = [512, 256, 128, 64, 32, 16]
#batch_list = [1, 4, 16, 16, 16, 16]
#prefix_list = ["visc1", "visc1", "visc1", "visc1", "visc1", "visc1", "visc1"]
#name_list = ["diff", "diff", "diff", "diff", "diff", "mean"]

#folder = "256"
#dim_list = [256, 128, 64, 32, 16]
#batch_list = [1, 4, 4, 4, 4]
#prefix_list = ["visc1", "visc1", "visc1", "visc1", "visc1", "visc1", "visc1"]
#name_list = ["diff", "diff", "diff", "diff", "mean"]

#folder = "128"
#dim_list = [128, 64, 32, 16]
#batch_list = [1, 1, 1, 1]
#prefix_list = ["visc1", "visc1", "visc1", "visc1", "visc1", "visc1", "visc1"]
#name_list = ["diff", "diff", "diff", "mean"]

#folder = "128"
#dim_list = [128, 64, 32, 16]
#batch_list = [1, 1, 1, 1]
#prefix_list = ["visc1", "visc1", "visc1", "visc1", "visc1", "visc1", "visc1"]
#name_list = ["diff", "diff", "diff", "mean"]

#folder = "64"
#dim_list = [64, 32, 16]
#batch_list = [1, 4, 4]
#prefix_list = ["visc1s", "visc1s", "visc1s", "visc1s", "visc1", "visc1", "visc1"]
#name_list = ["diff", "diff", "mean"]

folder = "32"
dim_list = [32, 16]
batch_list = [1, 1]
prefix_list = ["visc1s", "visc1s", "visc1s", "visc1", "visc1", "visc1", "visc1"]
name_list = ["diff", "mean"]

for i in range(0,len(dim_list)):
  size = dim_list[i]
  batch_size = batch_list[i]
  pattern = '{}*_{}_w_{}.bin'.format(prefix_list[i], size, name_list[i])
  
  batch_num = 0

  mean = None
  for file in os.listdir("./"):
    if fnmatch.fnmatch(file, pattern):
      print("Processing {}".format(file))
      n += 1
    
      data, = PetscBinaryIO().readBinaryFile(file)
      
      label = np.genfromtxt(file + ".info", delimiter=' ', usecols=0, dtype=str)
      value = np.genfromtxt(file + ".info", delimiter=' ', usecols=1, dtype=int)
      try:
        if label[0] == '-vecload_block_size':
          dof = value[0]
      except IndexError:
        dof = value
      
      if mean == None:
        mean = data
      else:
        mean += data
        
      batch += 1

    if batch == batch_size:
      print("Processed {} files out of {}".format(n, 0))
      mean /= float(n)
          
      for i in range(0,dof):
        ext = mean[i::dof].reshape(size, size)
        
        plt.clf()
        plt.pcolormesh(ext)
        plt.colorbar()
        if dof > 1:
          plt.savefig("{}/{}_{}_batch_{}_collect.png".format(folder, size, i, batch_num))
          np.save("{}/{}_{}_batch_{}_collect".format(folder, size, i, batch_num), ext)
        else:
          plt.savefig("{}/{}_batch_{}_collect.png".format(folder, size, batch_num))
          np.save("{}/{}_batch_{}_collect".format(folder, size, batch_num), ext)
      
      n = 0
      batch = 0
      mean = None
      batch_num += 1
      
    if batch_num >= batch_max:
      print("Batching this level completed...")
      break
      #break
      
print("Processed {} files...".format(n))
print("Processed {} batches...".format(batch_num))
