#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Utility to mass-plot data.

Description.

author: Filippo Leonardi
website: filippoleonardi.ch
last edited: January 2014
"""

# TODO: change plots, save in batch, etc...
import re

import PyQt5
from PyQt5.QtCore import Qt
from PyQt5 import QtWidgets, uic, QtGui

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

import sys
sys.path.append("../base")

from tools import *

import data
import hist
#import progress

class Display(QtWidgets.QMainWindow):
    def __init__(self):
        super(Display, self).__init__()

        self.glob_i = 0
        
        self.dt_list = []
        self.item_list = []
        self.parent_list_item = []
        self.parent_list = {}
        self.initUI()
        self.load_data()

        self.file_list.currentItemChanged.connect(self.redraw)

        self.open_action.triggered.connect(self.load_data)
        self.save_all_action.triggered.connect(self.save_all)

        self.opt_wid.accept_button.clicked.connect(self.filter)


    def initUI(self):

        self.stacked_figs = QtWidgets.QStackedWidget(self)

        self.prev_btn = QtWidgets.QPushButton("Previous")
        self.prev_btn.clicked.connect(self.previous_plot)
        self.next_btn = QtWidgets.QPushButton("Next")
        self.next_btn.clicked.connect(self.next_plot)

        self.vbox = QtWidgets.QVBoxLayout()
        self.fig = matplotlib.figure.Figure(figsize=(200, 200), dpi=100)
        self.canvas = FigureCanvas(self.fig)
        self.mpl_toolbar = NavigationToolbar(self.canvas, self)
        self.vbox.addWidget(self.mpl_toolbar)
        self.vbox.addWidget(self.canvas)

        self.hbox = QtWidgets.QHBoxLayout()

        self.hbox.addStretch(1)
        self.hbox.addWidget(self.prev_btn)
        self.hbox.addWidget(self.next_btn)

        self.vbox.addStretch(1)
        self.vbox.addLayout(self.hbox)
        self.centralwidget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.centralwidget)
        self.centralwidget.setLayout(self.vbox)

        self.init_docks()
        self.init_toolbar()
        self.init_statusbar()

        self.setMinimumWidth(800)
        self.setWindowTitle('Display')
        self.show()

    def init_docks(self):
        self.dock = QtWidgets.QDockWidget("File list", self);
        self.dock.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        self.file_list = QtWidgets.QTreeWidget(self.dock);
        self.file_list.setColumnCount(2);
        self.file_list.setHeaderLabels(["#","Filename"]);
        self.file_list.header().resizeSection(0, 70)
        self.dock.setWidget(self.file_list)

        self.options_dock = QtWidgets.QDockWidget("Options", self);
        self.options_dock.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        self.opt_wid = uic.loadUi("options_dock.ui")
        self.opt_wid.show()
        self.options_dock.setWidget(self.opt_wid)
        #self.addDockWidget(Qt.LeftDockWidgetArea, self.dock);
        #self.addDockWidget(Qt.LeftDockWidgetArea, self.options_dock);


        self.info_dock = QtWidgets.QDockWidget("Informations", self);
        self.info_dock.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        self.info_wid = uic.loadUi("info_dock.ui")
        self.info_wid.show()
        self.info_dock.setWidget(self.info_wid)


        self.tools_dock = QtWidgets.QDockWidget("Toolbar", self);
        self.tools_dock.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)

        self.addDockWidget(Qt.LeftDockWidgetArea, self.options_dock);
        self.tabifyDockWidget(self.options_dock, self.dock);

        self.addDockWidget(Qt.RightDockWidgetArea, self.info_dock);
        self.tabifyDockWidget(self.info_dock, self.tools_dock);


    def init_toolbar(self):
        self.toolbar = QtWidgets.QToolBar("Main tools", self)
        self.open_action = QtWidgets.QAction(QtGui.QIcon.fromTheme("document-open"), "Load", self)
        self.save_all_action = QtWidgets.QAction(QtGui.QIcon.fromTheme("document-save"), "Save all", self)
        self.toolbar.addAction(self.open_action)
        self.toolbar.addAction(self.save_all_action)
        self.addToolBar(self.toolbar)


    def init_statusbar(self):
        #self.statusbar = QtWidgets.QStatusBar(self)

        self.status_lbl = QtWidgets.QLabel()
        self.status_pbar = QtWidgets.QProgressBar()

        self.statusBar().addPermanentWidget(self.status_lbl)
        self.statusBar().addPermanentWidget(self.status_pbar)
        self.status_pbar.setMaximumWidth(150)

    def get_filters(self):
        flt = {}
        flt["sim"] = None
        flt["size"] = (-inf, inf)
        flt["comp"] = []
        flt["type"] = None
        flt["frame"] = (-inf, inf)

        if self.opt_wid.velo_filter_check.isChecked():
          flt["comp"].append("U")
        if self.opt_wid.vort_filter_check.isChecked():
          flt["comp"].append("w")

        return flt

    def load_data(self):

        file_types = "PETSc binary '.bin' (*.bin);;Numpy binary '.npy,.npz' (*.npy,*.npz);;Structured VtK '.vts'(*.vts)"
        filelist = QtWidgets.QFileDialog.getOpenFileNames(self, 'Load files', '', file_types)[0]

        i = 0
        self.status_pbar.setMaximum(len(filelist))
        self.status_lbl.setText("{} of {}".format(0,len(filelist)))
        for f in filelist:

          # FIXME: ugly workaround for auto type detection
          if "histo" in f:
                self.dt_list.append(hist.Hist(f))
          else:
                self.dt_list.append(data.Data(f, refactor = True, preload = False))


          parent = re.sub('_f[0-9]*','_f*', self.dt_list[-1].filename)
          if parent in self.parent_list.keys():
            lvi = self.parent_list_item[self.parent_list[parent]];
          else:
            lvi = QtWidgets.QTreeWidgetItem(self.file_list, ["#", parent]);
            lvi.setExpanded(True)
            self.parent_list_item.append(lvi)
            self.parent_list[parent] = len(self.parent_list_item) - 1

          self.item_list.append(QtWidgets.QTreeWidgetItem(lvi, [str(self.glob_i), self.dt_list[-1].filename]));

          self.glob_i += 1
          i += 1
          self.status_pbar.setValue(i)
          self.statusBar().showMessage("{} of {}".format(i,len(filelist)))

        self.statusBar().showMessage("Loaded {} files...".format(len(filelist)))

        #self.dt_list[-1].plot(self.fig, None)
        #self.current_idx = 0
        try:
          self.file_list.setCurrentItem(self.item_list[0])
        except:
          pass

        self.canvas.draw()

    def filter(self):
        flt = self.get_filters()

        i = 0
        for item in self.item_list:
          if not self.dt_list[i].match(flt):
             brush = QtGui.QBrush()
             color = QtGui.QColor(255, 0, 0)
             brush.setColor(color)
             item.setForeground(1, brush)
          else:
             brush = QtGui.QBrush()
             color = QtGui.QColor(0, 255, 0)
             brush.setColor(color)
             item.setForeground(1, brush)
          i += 1

    def save_all(self):
        i = 0
        for item in self.item_list:
          #self.file_list.setCurrentItem(self.item_list[i])
          self.redraw(self.item_list[i])
          self.canvas.print_figure(self.dt_list[i].name + ".png", dpi=200)
          i += 1
          ##if item is selected
          ##self.dt_list[i].load()import matplotlib
          #import matplotlib.pyplot as plt
          ##self.fig = matplotlib.figure.Figure(figsize=(200, 200), dpi=100)
          #self.dt_list[i].plot(self.fig, None)
          ##plt.show()
          #plt.savefig(self.dt_list[i].name + ".png")
          ##self.fig.show()
          ##fig.savefig(self.dt_list[i].name + ".png")
          #i += 1

    def update_info(self):
        #self.clear_labels()
        try:
            if isinstance(self.current_data(), data.Data):
                self.current_data().gen_info()
                self.info_wid.name_lbl.setText(self.current_data().filename)
                self.info_wid.comp_lbl.setText(self.current_data().comp_name)
                self.info_wid.typ_lbl.setText(self.current_data().type_name)
                self.info_wid.mesh_lbl.setText(self.current_data().size_name + "x" + self.current_data().size_name)
                self.info_wid.frame_lbl.setText(self.current_data().frame_name)
            if isinstance(self.current_data(), hist.Hist):
                self.info_wid.name_lbl.setText(self.current_data().name)
        except:
            pass

    def clean(self):
        pass

    def previous_plot(self):
        if self.current_idx > 0:
            self.current_idx -= 1
            self.file_list.setCurrentItem(self.item_list[self.current_idx ])

    def next_plot(self):
        if self.current_idx < len(self.dt_list) - 1:
            self.current_idx += 1
            self.file_list.setCurrentItem(self.item_list[self.current_idx ])

    def current_data(self):
        return self.dt_list[self.current_idx]

    def redraw(self, item):
        try:
          self.current_idx = int(item.text(0))
        except ValueError:
          return
        self.dt_list[self.current_idx].plot(self.fig, None)
        self.update_info()
        self.canvas.draw()
        #self.stacked_figs.setCurrentIndex(self.current_idx)

def main():

    app = QtWidgets.QApplication(sys.argv)

    win = Display()

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
