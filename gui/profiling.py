#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""


Description.

author: Filippo Leonardi
website: filippoleonardi.ch
last edited: January 2014
"""

import PyQt5
from PyQt5.QtCore import Qt
from PyQt5 import QtWidgets, uic, QtGui

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
#from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
#from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar

import glob

import sys
sys.path.append("../profiling")
sys.path.append("../base")

import profiling_parser
import allocation_parser

from tools import *

class ProfilingGUI(QtWidgets.QMainWindow):

    def __init__(self, size):
        super(ProfilingGUI, self).__init__()

        self.initUI()

        self.open_action.triggered.connect(self.load_data)
        self.save_all_action.triggered.connect(self.save_data)

    def initUI(self):

        #self.qlbl = QtWidgets.QLabel()
        #self.pbar = QtWidgets.QProgressBar(self)
        #self.pbar.setMaximum(self.size)

        #self.btn = QtWidgets.QPushButton('Start', self)

        #hbox = QtWidgets.QHBoxLayout()
        #hbox.addStretch(1)
        #hbox.addWidget(self.btn)

        self.vbox = QtWidgets.QVBoxLayout()

        self.fig = matplotlib.figure.Figure(figsize=(200, 200), dpi=100)
        self.canvas = FigureCanvas(self.fig)
        self.mpl_toolbar = NavigationToolbar(self.canvas, self)
        self.vbox.addWidget(self.mpl_toolbar)
        self.vbox.addWidget(self.canvas)

        self.centralwidget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.centralwidget)
        self.centralwidget.setLayout(self.vbox)
        self.init_toolbar()

        self.setMinimumWidth(500)
        self.setWindowTitle('Processing data...')
        self.show()

    def init_toolbar(self):
        self.toolbar = QtWidgets.QToolBar("Main tools", self)
        self.open_action = QtWidgets.QAction(QtGui.QIcon.fromTheme("document-open"), "Load", self)
        self.save_all_action = QtWidgets.QAction(QtGui.QIcon.fromTheme("document-save"), "Save all", self)
        self.toolbar.addAction(self.open_action)
        self.toolbar.addAction(self.save_all_action)
        self.addToolBar(self.toolbar)


    def load_data(self):
        file_types = "Allocation data file 'allocation.out' (*allocation.out)"
        file = QtWidgets.QFileDialog.getOpenFileNames(self, 'Save file', '', file_types)[0]

        tmp = file[0].split("_allocation.")[:-1]
        print("Matching pattern {0}".format(tmp[0] + "_profiling_rk*.py"))
        filelist = glob.glob(tmp[0] + "_profiling_rk*.py")
        print("Found {0} profiling files...".format(len(filelist)))

        #file_types = "Binary '.bin' (*.bin)"
        #filelist = QtWidgets.QFileDialog.getOpenFileNames(None, 'Save file', '', file_types)[0]

        self.alloc_data = allocation_parser.AllocatorParser()
        self.alloc_data.parse(file[0], sim_count = "log")

        print("Allocation file says {0} ranks were used, you provided {1} profiling data "
              "files".format(len(self.alloc_data.rk_list), len(filelist)))


        #self.rk_list = []
        #self.dom_list = []

        ##self.world_size = 0
        #self.num_levels = 0
        #self.intra_lvl_size = 0
        #self.intra_dom_size = 0

        self.prof_data = profiling_parser.ProfilerParser()
        self.prof_data.parse(self.alloc_data, filelist)
        self.prof_data.plot(self.fig)

        self.alloc_data.generate_data()
        self.alloc_data.plot(self.fig, weighted_sim = True)

        self.canvas.draw()

    def save_data(self):
        self.alloc_data.rescale()
        self.prof_data.rescale()

def main():

    app = QtWidgets.QApplication(sys.argv)

    win = ProfilingGUI(400)

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
