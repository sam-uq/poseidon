#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""


Description.

author: Filippo Leonardi
website: filippoleonardi.ch
last edited: January 2014
"""

from PyQt5 import QtWidgets

class Progress(QtWidgets.QWidget):
    
    def __init__(self, size):
        super(Progress, self).__init__()
        
        self.size = size
        self.curr = 0
        self.initUI()
        
    def initUI(self):
        
        self.qlbl = QtWidgets.QLabel()
        self.pbar = QtWidgets.QProgressBar(self)
        self.pbar.setMaximum(self.size)

        self.btn = QtWidgets.QPushButton('Start', self)
        
        hbox = QtWidgets.QHBoxLayout()
        hbox.addStretch(1)
        hbox.addWidget(self.btn)
        
        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.qlbl)
        vbox.addWidget(self.pbar)
        vbox.addStretch(1)
        vbox.addLayout(hbox)
        
        self.setLayout(vbox)
        
        self.setMinimumWidth(500)
        self.setWindowTitle('Processing data...')
        self.show()
        
    def next(self, name):
        self.qlbl.setText("Processing {} ({} of {})...".format(name, self.curr, self.size))
        self.pbar.setValue(self.curr)
        self.curr += 1
        
    def done(self):
        self.qlbl.setText("Done.")
        self.pbar.setValue(self.size)
