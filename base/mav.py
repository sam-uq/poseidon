#! /usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
from subprocess import check_output
import os

#from matplotlib import rc
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
#rc('text', usetex=True)

def parse_out(filename):
    runtime_cmd = 'less ' + filename + " | grep 'Run time' | sed 's/.*Run time :[ ]*\(.*\) sec.*/\\1/'"
    #print(runtime_cmd)
    runtime = float(check_output(runtime_cmd, shell=True))
    # print(runtime)

    maxp_cmd = 'less ' + filename + " | grep 'Max Processes' | sed 's/.*Max Processes :[ ]*\(.*\)[ ]*/\\1/'"
    # print(runtime_cmd)
    try:
        maxp = int(check_output(maxp_cmd, shell=True)) - 3
    except: 
        maxp = 1
        
        
    maxp_cmd = 'less ' + filename + " | grep 'splitted across ' | sed 's/.*splitted across[ ]*\(.*\)[ ]*ranks.*/\\1/'"
    #print(runtime_cmd)
    maxp1 = int(check_output(maxp_cmd, shell=True))
    if maxp != maxp:
        asdasdasd
    #print(maxp)
    #| grep 'Run time' | sed 's/.*Run time :[ ]*\(.*\) sec.*/\1/'
    #| grep 'Max Processes' | sed 's/.*Max Processes :[ ]*\(.*\)[ ]*/\1/'
    return runtime, maxp

class MaVPlot():
    def __init__(self, what):
        self.what = what
        self.mavs = []
        self.parameters = []
        self.param_names = []
        self.component_names =  []
        
    def push_mav(self, mav, params = None):
        self.mavs.append(mav)
        self.parameters.append(params)
        
    def set_params_names(self, names):
        self.param_names = names
        
    def set_component_names(self, names):
        self.component_names = names
        
    def plot(self, idx = None, loglog = False):
        if idx == None:
            xval = range(0, len(self.mavs))
            xname = "Index"
        else:
            xval = [p[idx] for p in self.parameters]
            xname = self.param_names[idx]
        mean = [m.get_mean() for m in self.mavs]
        std = [m.get_std() for m in self.mavs]
        yname = self.what
        title = self.what

        m = []
        s = []
        try:
            size = len(mean[0])
            for comp in range(0,size):
                m.append([mn[comp] for mn in mean])
                s.append([st[comp] for st in std])
        except ValueError:
            size = 1
            m.append(mean)
            s.append(std)
            
        for comp in range(0, size):
            print(m[comp])
            print(s[comp])
            print(xval)
            fig = plt.figure()
            ax = fig.add_subplot(1,1,1)
            ax.errorbar(xval, m[comp], yerr=s[comp], label="mean \pm std.")
            ax.set_title(title + " of " + self.component_names[comp] + " vs. " + xname)
            ax.set_xlabel(xname)
            ax.set_ylabel(yname)
            plt.legend()
            if loglog:
                ax.set_xscale("log", nonposx='clip')
                ax.set_yscale("log", nonposy='clip')
            plt.savefig("{}_{}_{}{}.png".format(self.what, comp, xname, loglog and "_loglog" or ""))
            plt.close()
            
    def save(self, folder):
        folderName = folder + ".mav"
        if os.path.exists(folderName):
            i = 0
            while os.path.exists(folderName):
                folderName = folder + "-" + str(i) + ".mav"
                i += 1
        os.makedirs(folderName)
                
        with open(folderName + '/info.dat', 'w') as data_file:
            data_file.write(self.what + "\n")
            data_file.write("--parameters--\n")
            for param in self.param_names:
                data_file.write(param + "\n")
            data_file.write("--components--\n")
            for param in self.component_names:
                data_file.write(param + "\n")
        
        np.save(folderName + '/mean.npy', [mav.get_mean() for mav in self.mavs])
        np.save(folderName + '/std.npy', [mav.get_std() for mav in self.mavs])
        for i in range(0,len(self.param_names)):
            np.save(folderName + '/param' + str(i) + '.npy', [param[i] for param in self.parameters])

class MaV():
    def __init__(self):
        self.n = 0
        self.mean = 0.
        self.m2 = 0.

    def push(self, sample):
        self.n += 1

        delta = sample - self.mean
        self.mean += delta / float(self.n)
        self.m2 += delta * (sample - self.mean)

    def get_mean(self):
        return self.mean

    def get_var(self):
        if self.n < 2:
            self.var = 0.
        else:
            self.var =  self.m2 / (float(self.n) - 1.)
        return self.var

    def get_std(self):
        self.std = np.sqrt(self.get_var())
        return self.std

def plot(mav_vec):
    #print(mav_vec[0].get_mean())
    mean = [m.get_mean() for m in mav_vec]
    std = [m.get_std() for m in mav_vec]

    plt.figure()
    plt.errorbar(x, y, xerr=0.2, yerr=0.4)
    plt.title("Simplest errorbars, 0.2 in x, 0.4 in y")


    print(mean)
    print(std)

