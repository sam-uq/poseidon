#! /usr/bin/env python2

# BASH CROP COMMAND  for f in *.pdf; do pdfcrop $f crop-$f; done;
import sys
import os.path
import re

import numpy as np
from ast import literal_eval

sys.path.append("./")
try:
    from base.tools import *
except:
    try:
        from poseidon.base.tools import *
    except:
        from tools import *

if not 'PETSC_DIR' in os.environ:
    error("Environment variable 'PETSC_DIR' not found!")
try:
    sys.path.append(os.environ['PETSC_DIR'] + "/bin/")
    from PetscBinaryIO import *
except ImportError:
    try:
        sys.path.pop()
        sys.path.append(os.environ['PETSC_DIR'] + "/bin/pythonscripts/")
        from PetscBinaryIO import *
    except ImportError:
        warning("Cannot find PETSc I/O module! Make sure 'PETSC_DIR' is set and contains the Python bindings. Loading .bin files will not work.")

try:
    import matplotlib as mpl
    mpl.use("Agg")
    import matplotlib.pyplot as plt
except ImportError:
    warning("Cannot find 'matplotlib', ignoring module, but plotting won't work!")

class Data:
  """
  The data class loads and manipulates data from disk.

  Class that contains one snapshot of a solution, does I/O in PETSc biary or numpy and provides some computation
  routine such as norm.
  """

  def __init__(self, source = None, shape = None, refactor = False, refactored = False,
               domain = None, preload = True, frame = 0, comps = [0], no_mesh = False):
    """
    If provided, loads a file named filename.

    If shape is provided, it will be used to properly interpret the data.

    If refactor is True, then the data is refactored to be of correct shape.
    If refactored is True, then the data is already refactored.
    If domain is provided, it will be used to hint the domain of the data.
    If preload is set to False, the only is loaded only when necessary (COU).
    If frame is provided, only that frame is loaded.
    If comps are provided, only those components are loaded from disk.
    If no_mesh is provided, then the mesh file is not available.

    Supporte file types are: xdmf (with hdf5 backend), vts, PETSc inary, numpy.
    Might also load data from other Data instance.
    """

    self.loaded = False

    self.source = source

    self.shape = None

    self.default_domain = False
    if domain == None:
        self.default_domain = True
        self.domain = [[0,1],[0,1]]
    else:
        self.domain = domain

    if type(source) is str:
        #print("File constructor.")
        self.complete_name = source
        self.filename = source.split("/")[-1]
    elif type(source) is Data or isinstance(source, Data):
        debug("Copying data...")
        if source.loaded:
            self.data = source.data.copy()
        self.loaded = source.loaded
        self.complete_name = source.complete_name
        self.filename = source.name
        self.shape = source.shape
        self.reshaped = source.reshaped
        self.deinterlaced = source.deinterlaced
        self.dof = source.dof
        self.dvolume = source.dvolume
        self.domain = source.domain
        self.dsize = source.dsize
        self.length = source.length
        self.dof = source.dof
        try:
            self.frame = source.frame
        except:
            pass
        try:
            self.time = source.time
        except:
            pass
        try:
            self.additions = source.additions
        except:
            pass
        try:
            self.sum_weights = source.sum_weights
        except:
            pass
        try:
            self.is3d = source.is3d
        except:
            pass
        try:
            self.size = source.size
        except:
            pass
    elif type(source) is np.array or isinstance(source, np.ndarray):
      #print("Numpy array constructor.")
      self.complete_name = "Numpy array"
      self.filename = "array"
    else:
      error("Unrecognized format {0}!!".format(type(source)))

    if preload:
      self.load(source, shape, refactor, refactored, frame, comps, no_mesh)
      self.setup()
    # TODO: list, tuple, etc, ...

  def load(self, source = None, shape = None, refactor = False, refactored = False, frame = 0, comps = [0], no_mesh = False):
    """
    Retrive the format of the file and calls the appropriate function.

    This is called by the constructor to dispatch the loading to the proper function.
    """
    if self.loaded:
        return
    output("Loading data...")
    self.loaded = True

    try:
        if source == None:
            source = self.source
    except ValueError:
        pass


    self.gen_info()
    if not no_mesh:
        self.try_load_mesh()

    if type(source) is str:
        ext = os.path.splitext(source)[1][1:]
        #print(ext)
        #print(source)
        if ext == "":
            valid_ext = ["bin", "xml", "xdmf", "npy", "npz", "vts", "vtk"]
            for ext_ in valid_ext:
                if os.path.exists(source + "." + ext_):
                    ext = ext_
                    source += "." + ext_
                    output("Detected extension '{0}'.".format(ext))
                    continue
        if not os.path.exists(source):
            error("File does not exists!")

        if ext == "bin":
            self.load_bin(source, shape, refactor)
        elif ext in ["xml", "xdmf"]:
            self.load_xdmf(source, shape, refactor, frame, comps)
        elif ext in ["npy", "npz"]:
            self.load_numpy(source, shape, refactor, refactored)
        elif ext in ["vts", "vtk"]:
            #error("VtK postprocessing is currently unsupported.")
            self.load_vtk(source, shape, refactor, refactored)
        else:
            error("Unrecognised file format extension '.{}'.".format(ext))
    elif type(source) is Data or isinstance(source, Data):
        self.load_data(source)
    elif type(source) is np.array or isinstance(source, np.ndarray):
        self.load_numpy(source)

  def try_load_mesh(self):
    try:
        self.filesplit2 = self.root.split("_")
        shape = [int(x) for x in self.filesplit2[1].split(",")]

        self.filesplit = self.filesplit2[0:2]
        self.filesplit.append("mesh")
        #self.filesplit.append(self.filesplit2[3])

        debug("Loading mesh with shape: {0}.".format(shape))
        self.mesh_file = (self.head == "" and "." or self.head) + "/" + "_".join(self.filesplit[0:4]) + ".bin"
        output("Try to load mesh data {0}...".format(self.mesh_file))
        msh, = PetscBinaryIO().readBinaryFile(self.mesh_file)

        #print(msh[0:-1:2])
        #print(msh[0:2:].shape)

        self.mesh_data = [msh[0::2].reshape(shape[:-1]), msh[1::2].reshape(shape[:-1])]
        self.has_mesh_attached = True
        #print(self.mesh_data)
        self.shape = shape[:-1]
    except:
        warning("No mesh data or broken mesh data!")
        self.has_mesh_attached = False


  def gen_info(self):
    self.gen_info_from_name()

    self.name = "unknown"
    self.head, self.tail = os.path.split(self.source)
    #print(self.head)
    #print(self.tail)
    self.root, self.ext = os.path.splitext(self.tail)
    self.head = self.head == "" and "." or self.head
    output("Locating MUFFALO at " + self.head + "/" + self.root + ".py")
    try:
        attrs = np.genfromtxt(self.head + "/" + self.root + ".py", delimiter='=', usecols=(0), dtype=str)
        vals = np.genfromtxt(self.head + "/" + self.root + ".py", delimiter='=', usecols=(1), dtype=str)
        for attr, val in zip(attrs, vals):
            setattr(self,attr,val)

        self.domain = literal_eval(self.domain)
        self.dof = int(self.dof)
        self.frame = int(self.frame)
        self.time = float(self.time)
        self.additions = int(self.additions)
        self.sum_weights = float(self.sum_weights)
        self.is3d = int(self.is3d)
        self.shape = np.array(self.size[1:-1].split(","), dtype=int)
        if not self.is3d:
            self.shape = self.shape[0:-1]
        output("Metadata hints to shape {0}".format(self.shape))
        return
    except OSError:
        warning("Could not find MUFFALO metadata, carrying on...")
    except IOError:
        warning("Could not find MUFFALO metadata, carrying on...")
    except AttributeError:
        warning("Broken MUFFALO metadata, skipping...")
    except ValueError:
        warning("Empty or invalid MUFFALO file?")
    if self.default_domain:
        warning("Using default domain!!!")

  def human_title(self,dof = None, time = None):
    title = ""
    #print(self.component)
    if self.type == "\"mean\"":
        title += "Mean of "
    elif self.type == "\"var\"":
        title += "Variance of "
    elif "sim" in self.type:
        try:
            title += "Sample {0} of ".format(self.sim_num)
        except:
            title += "Sample of "

    #title += " "

    if self.component == "\"U\"":
        title += "Velocity"
    elif self.component == "\"Gp\"":
        title += "Pressure"
    elif self.component == "\"w\"":
        title += "Vorticity"

    if dof and dof == 0:
        title += " (x-comp.)"
    if dof and dof == 1:
        title += " (y-comp.)"
    if dof and dof == 2:
        title += " (z-comp.)"

    if time:
        title += " at time t = {0}".format(t)

    #print(title)
    return title

  def load_vtk(self, filename, shape = None, refactor = False, frame = 0, comps = [0]):
    import vtk
    from vtk.util.numpy_support import vtk_to_numpy
    
    reader = vtk.vtkXMLStructuredGridReader()
    reader.SetFileName(filename)
    #reader.ReadAllScalarsOn()
    reader.Update()
    
    self.shape = reader.GetOutput().GetDimensions()
    if len(self.shape) >= 3 and self.shape[2] == 1:
        self.shape = self.shape[:-1]
    #print(self.shape)
    self.deinterlaced = True
    self.reshaped = False

    Ux = reader.GetOutput().GetPointData().GetArray("x")
    Uy = reader.GetOutput().GetPointData().GetArray("y")
    self.data = np.array([vtk_to_numpy(Ux), vtk_to_numpy(Uy)])
    #print(self.data.shape)

  ### TODO: grid nodes, edges etc...
  def load_xdmf(self, filename, shape = None, refactor = False, frame = 0, comps = [0]):
    """
    Load a XDMF file, manually parses the xml, and loads the hd5 file gluing the data automagically.
    """
    import xml.etree.ElementTree
    import h5py
    
    w_frame = frame
    w_comps = comps
    
    tree = xml.etree.ElementTree.parse(filename).getroot()
    
    frame_found = False
    
    for frame in tree.find("Domain").find("Grid").findall("Grid"):
        assert frame.get("GridType") == "Collection"
        if frame.get("Name") != "f" + str(w_frame):
            continue
        else:
            frame_found = True
        self.data = None
        for part in frame.findall("Grid"):
            assert part.get("GridType") == "Uniform"
            dset_part = None
            not_have_part = True
            for (i, comp) in enumerate(part.findall("Attribute")):
                assert comp.get("Center") == "Node"
                if i not in w_comps:
                    continue
            
                piece = comp.find("DataItem")
                h5name = piece.text.split(":/")[0]
                h5comp = piece.text.split(":/")[1]
                piece_shape = tuple([int(s) for s in piece.get("Dimensions").split(" ")[0:2]])
                debug("Loading (shape = {1}: {0} ...".format(h5name, piece_shape))
                
                with h5py.File(h5name,'r') as f:
                    if f is None:
                        error("hdf5 file not found")
                    
                    dset_comp = f.get(h5comp)[0:piece_shape[0],0:piece_shape[1]]
                    # 
                    if not_have_part and dset_part == None:
                        dset_part = dset_comp
                        not_have_part = False
                    else:
                        dset_part = np.dstack([dset_part, dset_comp])
                        not_have_part = False
                
                debug("Loading component {}".format(h5comp))
            if self.data is None:
                self.data = dset_part
            else:
                self.data = np.vstack([self.data, dset_part])
                
    if frame_found == False:
        error("Requested frame not found!", FrameNotFoundException)
    
    self.shape = np.array([0,0])
    for topo in tree.find("Domain").findall("Topology"):
        local_dim = [int(i) for i in topo.get("Dimensions").split(" ")[0:2]]
        self.shape[0] += local_dim[0]
        self.shape[1] = local_dim[1]
    self.shape = tuple(self.shape[::-1])
    
    self.dof = len(w_comps)
    if self.dof > 1:
        self.data = np.swapaxes(self.data,0,2)
    else:
        self.data = np.array([self.data])
    
    self.deinterlaced = True
    self.reshaped = True

  def load_bin(self, filename, shape = None, refactor = False):
    """
    Load a PETSc binary file, initializing some additional data. By default a PETSc binary is loaded as interlaced
    linear array. By befault no deinterlacing/reshping is made (for efficiency reasons) and PETSc binary doesn't care
    about data shape (the data will be an interlaced 1-d array). If refactor is set to True the the data is
    deinterlaced and reshaped to a (ndofs, nx, ny, ...) array.
    """

    try:
        self.data, = PetscBinaryIO().readBinaryFile(filename)
    except IOError:
        _classid = {1211216:'Mat',
                    1211214:'Vec',
                    1211218:'IS',
                    1211219:'Bag'}

        indices = 64
        if indices == 64:
            _inttype = np.dtype('>i8')
        else:
            _inttype = np.dtype('>i4')
        header = int(np.fromfile(filename, dtype=_inttype, count=1)[0])
        error("Error while loading binary vector: " + filename + "(header = " + str(header) + "==>" + _classid[header] + "), maybe wrong arch?")
        
        
        #try:
        obj = PetscBinaryIO()
        obj._inttype = np.dtype('>i8')
        self.data, = obj.readBinaryFile(filename)
        print(self.data)

    try:
      label = np.genfromtxt(filename + ".info", delimiter=' ', usecols=(0), dtype=str)
      value = np.genfromtxt(filename + ".info", delimiter=' ', usecols=(1), dtype=int)
    except:
      warning("the .info file could not be read, continuing assuming 1 dof...")
      label = ['-vecload_block_size']
      value = 1
    try:
      if label[0] == '-vecload_block_size':
        self.dof = value[0]
    except IndexError:
      self.dof = value
    except TypeError:
      self.dof = value

    self.deinterlaced = False
    self.reshaped = False

    #print(self.shape)
    if refactor:
        if self.shape == None:
            self.guess_shape(shape)
        if refactor == "deinterlace":
            self.deinterlace()
        else:
            self.refactor()

  def load_numpy(self, source, shape = None, refactor = False, refactored = False):
    """
    Load data as numpy data. By default data in numpy format is saved using PETSc convention. If the data was saved
    after a refactor then you must specity that using the refactored flag. Data is refactored by passing the flag
    refactor.
    """


    if type(source) is str:
      self.data = np.load(source)
    else:
      self.data = source

    if len(self.data.shape) == 3:
      self.dof = self.data.shape[0]
      self.shape = self.data.shape[1:]
    elif len(self.data.shape) == 2:
      # TODO: this should guess better, two cases, either (dof, x*y) or (x, y)
      # IDEA 1: use ratio (dof< 3)
      # idea 2: use comp name
      # in the second case we may want
      # y = np.expand_dims(a, axis=0)
      self.dof = 1
      self.shape = self.data.shape

    if refactored:
      self.deinterlaced = True
      self.reshaped = True
    else:
      self.deinterlaced = False
      self.reshaped = False

    self.guess_shape(shape)

    if refactor:
      self.refactor()

    #print("Loading numpy array from {0}, data is the following:".format(source))
    #print(" - loaded: {0}".format(self.loaded))
    #print(" - deinterlaced: {0}".format(self.deinterlaced))
    #print(" - reshaped: {0}".format(self.reshaped))
    #print(" - dofs: {0}".format(self.dof))
    #print(" - shape: {0}".format(self.shape))


  def load_data(self, otherdata):
    """
    Copy constructor.
    """
    self.data = np.copy(otherdata.data)
    self.deinterlaced = otherdata.deinterlaced
    self.reshaped = otherdata.reshaped
    self.dof = otherdata.dof
    self.shape = otherdata.shape

  def save(self, filename = None, datatype = "numpy"):
    """
    Save data onto a file named filename, using the datatype format.
    """
    if filename == None:
        filename = ".".join(self.filename.split(".")[0:-1]) + ".npy"
    np.save(filename, self.data)

  def setup(self):
    """
    Computes data such as volume and sizes.
    """
    #if not self.domain:
    #a = np.diff(self.domain)
    #print(np.array(self.domain))
    #self.domain = np.array([[0,1],[0,1],[0,1]]) ### FIXME!!!!!!
    #print(np.array(self.domain))
    output("Setting up data!")
    try:
        self.length = np.diff(np.copy(np.array(self.domain)))
    except:
        try:
            self.length = [self.domain[0][1] - self.domain[0][0], self.domain[1][1] - self.domain[1][0], self.domain[2][1] - self.domain[2][0]]
        except:
            self.length = [self.domain[0][1] - self.domain[0][0], self.domain[1][1] - self.domain[1][0], 0]
    #print(self.domain)
    self.length.reshape(len(self.domain))
    try:
        self.dsize = np.array(self.length).flatten().astype(float) / np.array(self.shape).flatten().astype(float)
    except:
        self.dsize = np.array(self.length).flatten()[0:2].astype(float) / np.array(self.shape).flatten()[0:2].astype(float)
    self.dvolume = np.prod(self.dsize.astype(float))
    #print(self.dvolume, self.domain, self.length, self.dsize, self.shape)

  def gen_mesh(self):
    """
    Setup appropriate mesh coordinates and meshgrid for plot. Not done automatically becasuse of reasons.
    """

    if self.has_mesh_attached:
        self.x = self.mesh_data[0][0,:]
        self.y = self.mesh_data[1][:,0]

        #plt.plot(self.x)
        #plt.plot(self.y)
        #plt.show()

        (self.meshx, self.meshy) = self.mesh_data
        #(self.meshx, self.meshy) = np.meshgrid(self.x, self.y)

        #self.meshx = self.meshx.T
        #self.meshy = self.meshy.T
    else:
        debug("Generating a mesh of size {0}".format(self.shape))
        self.x = np.linspace(self.domain[0][0] + .5*self.dsize[0], self.domain[0][1] - .5*self.dsize[0], self.shape[0])
        self.y = np.linspace(self.domain[1][0] + .5*self.dsize[1], self.domain[1][1] - .5*self.dsize[1], self.shape[1])

        (self.meshx, self.meshy) = np.meshgrid(self.x, self.y)
        #print(self.meshx.shape, self.meshy.shape)

  def refactor(self):
    """
    The Data is deinterlaced and reshaped
    according to a natural (dof, nx, ny, ...) format.
    """
    if not self.deinterlaced:
        self.deinterlace()
    if not self.reshaped:
        self.reshape()

  def deinterlace(self):
    """
    Deinterlace degrees of freedom from the data, mapping (a_1, b_1, ..., z_1, a_2, ...) to ((a_1, a_2, ..), ..., (z_1,
    ...))
    """
    if self.deinterlaced:
      warning("Seems data is already deinterlaced.")
      return

    temp = []
    for of in range(0, self.dof):
      temp.append(self.data[of::self.dof])
    self.data = np.array(temp)

    self.deinterlaced = True

  def reshape(self):
    """
    Data is reshaped to a natural layout, i.e. from 1-d array to a natural array.
    """
    if self.reshaped:
      warning("Seems data is already reshaped.")
      return
    if not self.deinterlaced:
      warning("Works only on deinterlaced data.")
      return

    try:
      self.data = self.data.reshape((self.dof, self.shape[1], self.shape[0]))
      self.reshaped = True
    except ValueError:
      warning("Could not reshape data!")

  def guess_shape(self, shape = None):
    """
    Try and see if you can guess the shape of the data (assuming perfect square).
    """


    print("Guessing shape...")
    # TODO: use shape suggestion

    if len(self.data.shape) == 1:
      s = int(np.sqrt(self.data.shape[0] / self.dof))
      self.shape = (s, s)

      self.reshaped = False
      self.deinterlaced = False
    elif len(self.data.shape) == 2:
      ## TODO FIXME
      self.dof = 1
      self.shape = self.data.shape
      self.reshaped = True
      self.deinterlaced = True
    elif len(self.data.shape) == 3:
      self.dof = self.data.shape[0]
      s1 = self.data.shape[1]
      s2 = self.data.shape[2]
      self.shape = (s1, s2)

      self.reshaped = True
      self.deinterlaced = True
    else:
      error("Seems something went wrong with the shape-finding stuff.")


    output("The shape seems to be {0} (on dof = {1}).".format(self.shape,self.dof))

  def validate(self, otherdata):
    """
    Checks data data is compatible for standard operators.
    """

    if not self.reshaped or not self.deinterlaced:
      warning("LHS Data must be refactored. Refactoring...")
      self.refactor()
      #return False
    if not otherdata.reshaped or not otherdata.deinterlaced:
      warning("RHS Data must be refactored. Refactoring...")
      otherdata.refactor()
      #return False
    if self.dof != otherdata.dof:
      warning("Data must have the same dof [{} != {}].".format(self.dof, otherdata.dof))
      return False
    for i in range(0,1):
      if ( self.shape[i] % otherdata.shape[i] != 0 or self.shape[i] % otherdata.shape[i] != 0 ) and \
         ( otherdata.shape[i] % self.shape[i] != 0 or otherdata.shape[i] % self.shape[i] != 0 ):
        warning("Invalid shape ratio [{} and {}].".format(self.shape[i], otherdata.shape[i]))
        return False

    return True

  def interpolate(self, shape, inplace=False, interptype = "volume"):
    """
    Interpolate this data to a finer shape and if inplace overrides the result,
    otherwise return *only numpy data*. Type of interpolation may be choosen among:
    - "volume": no interpolation
    - "spline": fast rectbivariatespline
    - "":
    - "cubic": interp2d cubic interpolation (warning: very slow)
    """

    (ratio_x, ratio_y) = shape / np.array(self.shape)
    data = []

    for i in range(0, self.dof):
      if interptype == "volume":
        data.append(np.zeros(shape))

        for of1 in range(0,int(ratio_x)):
          for of2 in range(0,int(ratio_y)):
            data[i][of1::int(ratio_x), of2::int(ratio_y)] = self.data[i]

      else:
        try:
          import scipy.interpolate
        except ImportError:
          warning("Scipy was not found in your installation, cannot use this feature.")
          return

        self.gen_mesh()
        if interptype == "spline":
          f = scipy.interpolate.RectBivariateSpline(self.x, self.y, self.data[i])
        else:
          f = scipy.interpolate.interp2d(self.x, self.y, self.data[i], kind=interptype)

        xnew = np.linspace(self.domain[0][0] + .5*self.dsize[0]/ratio_x, self.domain[0][1] - .5*self.dsize[0]/ratio_x, shape[0])
        ynew = np.linspace(self.domain[1][0] + .5*self.dsize[1]/ratio_y, self.domain[1][1] - .5*self.dsize[1]/ratio_y, shape[1])
        data.append(f(xnew, ynew))

    data = np.array(data)

    if inplace:
      self.shape = shape
      self.setup()
      self.data = data
    else:
      return data

  def __eq__(self, rhs):
    """
    Check if shapes are identical.
    """

    if type(rhs) is Data or isinstance(rhs, Data):
        if self.shape[0] == rhs.shape[0] and self.shape[1] == rhs.shape[1]:
            try:
                if np.array(self.domain) != np.array(rhs.domain):
                    warning("Data has same shape but different domain!")
            except ValueError:
                warning("Something fishy with domains: {0} - {1}".format(self.domain, rhs.domain))
            if (np.array(self.dsize) != np.array(rhs.dsize)).any():
                warning("Data has same shape but different dsize!")
            return True
        else:
            return False
    else:
        try:
            return this == rhs
        except:
            return False


  def __ne__(self, rhs):
    """
    Check if shapes are different.
    """

    return not (self == rhs)

  def __le__(self, rhs):
    """
    Shape comparison, check if one shape is smaller (or equal) than the other. May be none if shapes are not commensurable.
    """

    if self.shape[0] <= rhs.shape[0] and self.shape[1] <= rhs.shape[1]:
      return True
    elif self.shape[0] > rhs.shape[0] and self.shape[1] > rhs.shape[1]:
      return False
    return None

  def __ge__(self, rhs):
    """
    Shape comparison, check if one shape is greather (or equal) than the other.
    """

    return rhs <= self and rhs != self

  def __lt__(self, rhs):
    """
    Shape comparison, check if one shape is smaller (not equal) than the other.
    """

    return self <= rhs and rhs != self

  def __gt__(self, rhs):
    """
    Shape comparison, check if one shape is greather (not equal) than the other.
    """

    return self >= rhs and rhs != self

  def abs(self):
      ret = Data(self)
      ret.data = np.abs(self.data)
      return ret
      
  def __add__(self, rhs):
    """
    Add data from two different Data classes: if shapes are compatible but different, the smaller is interpolated to the finer.
    """

    if type(rhs) is Data or isinstance(rhs, Data):
      self.validate(rhs)

      if self < rhs:
        ret = Data(rhs)
        ret.data += self.interpolate(rhs.shape)
      elif self > rhs:
        ret = Data(self)
        ret.data += rhs.interpolate(self.shape)
      elif self == rhs:
        ret = Data(self)
        ret.data += rhs.data

    else:
        ret = Data(self)
        ret.data += rhs

    return ret

  def __sub__(self, rhs):
    """
    Add data from two different Data classes: if shapes are compatible but different, the smaller is interpolated to the finer.
    """

    if type(rhs) is Data or isinstance(rhs, Data):
      self.validate(rhs)

      if self < rhs:
        ret = Data(self.interpolate(rhs.shape))
        ret.data -= rhs.data
      elif self > rhs:
        ret = Data(self)
        ret.data -= rhs.interpolate(self.shape)
      elif self == rhs:
        ret = Data(self)
        ret.setup()
        ret.data -= rhs.data

    else:
        ret = Data(self)
        ret.data -= rhs

    return ret

  def __mul__(self, rhs):
    """
    Add data from two different Data classes: if shapes are compatible but different, the smaller is interpolated to the finer.
    """

    if type(rhs) is Data or isinstance(rhs, Data):
      self.validate(rhs)

      if self < rhs:
        ret = Data(rhs)
        ret.data *= self.interpolate(rhs.shape)
      elif self > rhs:
        ret = Data(self)
        ret.data *= rhs.interpolate(self.shape)
      elif self == rhs:
        ret = Data(self)
        ret.data *= rhs.data
    else:
        ret = Data(self)
        ret.data *= rhs

    return ret

  def __div__(self, rhs):
    """
    Add data from two different Data classes: if shapes are compatible but different, the smaller is interpolated to the finer.
    """

    if type(rhs) is Data or isinstance(rhs, Data):
      self.validate(rhs)

      if self < rhs:
        print("Errror!")
        ret = Data(rhs)
        ret.data /= self.interpolate(rhs.shape)
      elif self > rhs:
        ret = Data(self)
        ret.data /= rhs.interpolate(self.shape)
      elif self == rhs:
        ret = Data(self)
        ret.data /= rhs.data

    else:
        ret = Data(self)
        ret.data /= rhs

    return ret

  def __truediv__(self, rhs):
    """
    Add data from two different Data classes: if shapes are compatible but different, the smaller is interpolated to the finer.
    """

    if type(rhs) is Data or isinstance(rhs, Data):
      self.validate(rhs)

      if self < rhs:
        print("Errror!")
        ret = Data(rhs)
        ret.data /= self.interpolate(rhs.shape)
      elif self > rhs:
        ret = Data(self)
        ret.data /= rhs.interpolate(self.shape)
      elif self == rhs:
        ret = Data(self)
        ret.data /= rhs.data

    else:
        ret = Data(self)
        ret.data /= rhs

    return ret

  def __iadd__(self, rhs):
    """
    Inplace (better) add data from two different Data classes: if shapes are compatible but different, the smaller is interpolated to the finer.
    """

    if type(rhs) is Data or isinstance(rhs, Data):
      self.validate(rhs)

      if self < rhs:
        self.interpolate(rhs.shape, inplace=True)
        self.data += rhs.data
      elif self > rhs:
        self.data += rhs.interpolate(self.shape)
      elif self == rhs:
        self.data += rhs.data
    else:
        self.data += rhs

    return self

  def __isub__(self, rhs):
    """
    Inplace (better) add data from two different Data classes: if shapes are compatible but different, the smaller is interpolated to the finer.
    """

    if type(rhs) is Data or isinstance(rhs, Data):
      self.validate(rhs)

      if self < rhs:
        self.interpolate(rhs.shape, inplace=True)
        self.data -= rhs.data
      elif self > rhs:
        self.data -= rhs.interpolate(self.shape)
      elif self == rhs:
        self.data -= rhs.data
    else:
        self.data -= rhs

    return self

  def __imul__(self, rhs):
    """
    Inplace (better) add data from two different Data classes: if shapes are compatible but different, the smaller is interpolated to the finer.
    """

    if type(rhs) is Data or isinstance(rhs, Data):
      self.validate(rhs)

      if self < rhs:
        self.interpolate(rhs.shape, inplace=True)
        self.data *= rhs.data
      elif self > rhs:
        self.data *= rhs.interpolate(self.shape)
      elif self == rhs:
        self.data *= rhs.data
    else:
        self.data *= rhs

    return self

  def __idiv__(self, rhs):
    """
    Inplace (better) add data from two different Data classes: if shapes are compatible but different, the smaller is interpolated to the finer.
    """

    if type(rhs) is Data or isinstance(rhs, Data):
      self.validate(rhs)

      if self < rhs:
        self.interpolate(rhs.shape, inplace=True)
        self.data /= rhs.data
      elif self > rhs:
        self.data /= rhs.interpolate(self.shape)
      elif self == rhs:
        self.data /= rhs.data
    else:
        self.data /= rhs

    return self

  def __itruediv__(self, rhs):
    """
    Inplace (better) add data from two different Data classes: if shapes are compatible but different, the smaller is interpolated to the finer.
    """

    if type(rhs) is Data or isinstance(rhs, Data):
      self.validate(rhs)

      if self < rhs:
        self.interpolate(rhs.shape, inplace=True)
        self.data /= rhs.data
      elif self > rhs:
        self.data /= rhs.interpolate(self.shape)
      elif self == rhs:
        self.data /= rhs.data
    else:
        self.data /= rhs

    return self

  def op(self):
    #TODO fix above by passing operation to a general func
    pass

  def iop(self):
    #TODO fix above by passing operation to a general i-func
    pass

  def eval(self, x, y, dof = 0):
    """
    Evaluate the data at the point (x,y).
    """
    frac_x = (x - self.domain[0][0]) / (self.domain[0][1] - self.domain[0][0])
    frac_y = (y - self.domain[1][0]) / (self.domain[1][1] - self.domain[1][0])

    if self.reshaped and self.deinterlaced:
      return self.data[dof][int(frac_y * self.shape[1])][int(frac_x * self.shape[0])]
    elif self.deinterlaced and not self.reshaped:
      return self.data[dof][int(frac_y * self.shape[1]) * self.shape[0] + int(frac_x * self.shape[0])]
    else:
      return self.data[self.dof * (int(frac_y * self.shape[1]) * self.shape[0] + int(frac_x * self.shape[0])) + dof]

  def slice(self, coord = None, dir=0, dof=0):
      if coord == None:
        if self.reshaped and self.deinterlaced:
            print(self.data[dof].shape)
            return np.sum(self.data[dof], dir) / self.data[dof].shape[dir]
        elif self.deinterlaced and not self.reshaped:
            #return self.data[dof][int(frac_x * self.shape[0]):self.shape[0]:]
            pass
        else:
            #return self.data[int(frac_x * self.shape[0])) + dof:self.dof * self.shape[0]:]
            pass
      elif dir == 0:
        frac_x = (coord - self.domain[0][0]) / (self.domain[0][1] - self.domain[0][0])
        if self.reshaped and self.deinterlaced:
            return self.data[dof][:][int(frac_x * self.shape[0])]
        elif self.deinterlaced and not self.reshaped:
            return self.data[dof][int(frac_x * self.shape[0]):self.shape[0]:]
        else:
            return self.data[int(frac_x * self.shape[0]) + dof:self.dof * self.shape[0]:]
      #pass

  def norm(self, p = 2., vectornorm = False):
    """
    Compute the L_p norm on each dof.
    """
    #print(self.dvolume)
    p = float(p)
    if (self.dvolume < np.finfo(np.float64).eps or self.dvolume == 0):
        error("self.dvolume is zero!!")
        print(self.dvolume, self.domain, self.length, self.dsize, self.shape)
    
    if vectornorm:
        if not self.deinterlaced:
            warning("Works only on deinterlaced data.")
            return

        if p == 0:
            debug("Vector (l2) L 0 norm...")
            return self.shape
        elif type(p) is str or p >= inf or p == None or p < 0:
            debug("Vector (l2) L inf norm...")
            if self.reshaped:
                return np.max(np.sqrt(np.sum(np.power(self.data, 2.), 0)), (0, 1))
            else:
                return np.max(np.sqrt(np.sum(np.power(self.data, 2.), 0)), 0)

        debug("Computing vector (l2) L^{0} norm...".format(p))
        if self.reshaped:
            return np.sum(np.power(np.sum(np.power(self.data,2.),0), p/2.) * self.dvolume, (0, 1))**(1/p)
        else:
            return np.sum(np.power(np.sum(np.power(self.data,2.),0), p/2.) * self.dvolume, 0)**(1/p)
        

    if not self.deinterlaced:
      warning("Works only on deinterlaced data.")
      return

    if p == 0:
      debug("Computing L 0 norm...")
      return self.shape
    elif type(p) is str or p >= inf or p == None or p < 0:
      debug("Computing L inf norm...")
      if self.reshaped:
        return np.max(np.abs(self.data), (1, 2))
      else:
        return np.max(np.abs(self.data), 1)

    debug("Computing L^{0} norm...".format(p))
    if self.reshaped:
        return np.power(np.sum(np.power(np.abs(self.data), p)* self.dvolume, (1, 2)),1./p)
    else:
        return np.power(np.sum(np.power(np.abs(self.data), p) * self.dvolume, 1),1./p)

    

  def plot(self, fig = None, axes = None, dof = None, title = None, show = False, save = False,
            notitle=False, onlydof=None, time = None, dpi = None, cmap = None):
    """
    Plot the pcolor of the data.

    onlydof: list of dof you select, use no repeats and choose only existing Dofs. (no check performed so far).
    """

    self.fig_back = None
    if fig == None or isinstance(fig, str):
        self.fig_back = fig
        import matplotlib.pyplot as plt
        fig = plt.figure()

    fig.clf()


    if not self.loaded:
      self.load(refactor = True)
      self.setup()
    elif not (self.reshaped and self.deinterlaced):
        self.refactor()
        output("Data not refactored...")
    elif not self.reshaped:
        self.reshape()
        output("Data not reshaped...")
    elif not self.deinterlaced:
        self.deinterlace()
        output("Data not deinterlaced...")

    try:
      import matplotlib.pyplot as plt
    except ImportError:
      warning("Matplotlib was not found in your installation, cannot use this feature.")
      return
    self.gen_mesh()

    quiver  = False
    energy = False
    quiver  = False
    #plt.figure()
    if quiver:
        axes = fig.gca()
        #print(self.data[0], self.data[1])
        #ax = fig.add_axes([-1,1,-1,1])
        M = np.hypot(self.data[0], self.data[1])
        #im = axes.quiver(self.meshx, self.meshy, self.data[0], self.data[1], M,
        im = axes.quiver(self.meshx, self.meshy, self.data[0] / M, self.data[1] / M, M,
               #units='x',
               #pivot='tip',
               width=0.001,
               #scale=10)
               scale=100)
               #color='r', units='x',
               #linewidths=(1,), edgecolors=('k'), headaxislength=5)
        # , 'AutoScale', 'off') #, 'AutoScaleFactor',
        #10)
    elif energy:
        axes = fig.gca()
        #print(self.data[0], self.data[1])
        #ax = fig.add_axes([-1,1,-1,1])
        M = np.hypot(self.data[0], self.data[1])
        #im = axes.quiver(self.meshx, self.meshy, self.data[0], self.data[1], M,
        #im = axes.quiver(self.meshx, self.meshy, self.data[0] / M, self.data[1] / M, M,
               ##units='x',
               ##pivot='tip',
               #width=0.001,
               ##scale=10)
               #scale=100)
               ##color='r', units='x',
               ##linewidths=(1,), edgecolors=('k'), headaxislength=5)
        im = axes.pcolormesh(self.meshx, self.meshy, np.multiply(self.data[0],self.data[0]) + np.multiply(self.data[1],self.data[1]), vmin=0, vmax=8)
        fig.colorbar(im)
        # , 'AutoScale', 'off') #, 'AutoScaleFactor',
        #10)
    else:
        if onlydof:
            dof = len(onlydof)
        else:
            dof  = self.dof
            onlydof = range(0,dof)

        a,b = rectangularize(dof)
        debug("Rectngulatizing " + str(dof) + " "  + str(a) + " " + str(b))
        for i in onlydof:
            #self.f = plt.figure()
            if dof > 1:
                axes = fig.add_subplot(a,b,i+1)
            else:
                axes = fig.gca()

            axes.set_xlabel("x")
            axes.set_ylabel("y")
            axes.set_xlim(self.domain[0])
            axes.set_ylim(self.domain[1])
            axes.set_aspect('equal')
            axes.set_title(i)
            im = axes.pcolormesh(self.meshx, self.meshy, self.data[i], cmap= cmap)
            fig.colorbar(im)

            if notitle:
                pass
            elif title == None:
                try:
                    axes.set_title(self.human_title(dof=i,time=time))
                except:
                    axes.set_title(self.filename)
            else:
                axes.set_title(title[i])

            axes.set_xlim(self.domain[0])
            axes.set_ylim(self.domain[1])
            axes.set_aspect('equal')
    fig.patch.set_alpha(0)

    #plt.show(block=False)
    if show:
      plt.show(block=False)

    if isinstance(self.fig_back, str) or save == True:
        if self.fig_back == None:
            self.fig_back =  ".".join(self.filename.split(".")[0:-1]) + ".png"
        if dpi is None:
            plt.savefig(self.fig_back, cmap= cmap)
        else:
            plt.savefig(self.fig_back, dpi = dpi, cmap = cmap)
        plt.close()

  def gen_info_from_name(self):
    try:
        split = "".join(self.filename.split(".")[0:-1]).split("_")
        self.sim_name = split[0]
        self.size_name = split[1]
        self.comp_name = split[2]
        self.type_name = split[3]
    except:
        self.sim_name = "unknown"
        self.size_name = "unknown"
        self.comp_name = "unknown"
        self.type_name = "unknown"
    try:
      self.frame_name = split[4]
    except:
      self.frame_name = None

  def match(self, flt):
    self.gen_info_from_name()

    if not ( self.list_match(flt["sim"], self.sim_name) and
      self.list_match(flt["comp"], self.comp_name) and
      self.list_match(flt["type"], self.type_name) ):
      return False

    try:
      r = int(self.size_name)
      if not (flt["size"][0] < r and r <= flt["size"][1]):
        return False
    except:
      pass

    try:
      r = int(self.frame_name[1:])
      if not (flt["frame"][0] < r and r <= flt["frame"][1]):
        return False
    except:
      pass

    return True

  def list_match(self, patterns, string):
    """
    ???
    """
    if patterns == None:
        print("none")
        return True
    else:
        for p in patterns:
            if re.match(p, string):
                print("match")
                return True
        return False

