#! /usr/bin/env python

import math

try:
  inf = float("int")
except:
  inf = 1e310

EXIT_ON_ERROR=False
OUTPUT_LEVEL = 2
OUTPUT_DEBUG = 1
OUTPUT_STANDARD = 1
OUTPUT_WARNING = 1
OUTPUT_ERROR = 0

def debug(string):
    if(OUTPUT_LEVEL >= OUTPUT_DEBUG):
        print("[-DEBUG-] " + string)
        
def output(string):
    if(OUTPUT_LEVEL >= OUTPUT_STANDARD):
        print("\x1b[32;1m[OUTPUT ]\x1b[0m " + string)
        
def warning(string):
    if(OUTPUT_LEVEL >= OUTPUT_WARNING):
        print("\x1b[33;1m[WARNING]\x1b[0m " + string)
        
class FrameNotFoundException(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

def error(string, raiseex = False):
    if raiseex:
        print("\x1b[31;1m[-ERROR-]\x1b[0m " + string)
        raise raiseex(string)
    if(OUTPUT_LEVEL >= OUTPUT_ERROR):
        print("\x1b[31;1m[-ERROR-]\x1b[0m " + string)
    if EXIT_ON_ERROR:
        exit
  
def rectangularize(i):
    for j in reversed(range(1,int(math.floor(math.sqrt(i)+1)))):
         if i % j == 0:
           return (j, int(i / j))
