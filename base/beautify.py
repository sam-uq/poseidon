#! /usr/bin/env python

import numpy as np

def table_print(table, header = None, hspace = [8,8, None, 13], min_hspace = None, precision = None, preset = None):
    hsep_head = ["", " ", ""]
    hsep = ["", " ", ""]
    hline =[False, False, False]
    last_sep = "\n"
    preprint = None
    postprint = None
    # TODO: border
    if preset == "LaTeX":
        hsep_head = ["", " & ", " \\\\"]
        hsep_line = ["\hline"]
        hsep = ["", " & ", " \\\\"]
        hline =[True, True, False]
        supress_last_sep = True
    elif preset == "CSV":
        hsep_head = ["", ", ", ""]
        hsep = ["", ", ", ""]
        hline =[False, False, False]
    elif preset == "PyArray":
        preprint = "["
        hsep_head = ["[", ",", "],"]
        hsep = ["[", ",", "],"]
        last_sep = "]\n"
        postprint = "]"
        hline =[False, False, False]
    elif preset == "CArray":
        preprint = "{"
        hsep_head = ["{", ",", "},"]
        hsep = ["{", ",", "},"]
        last_sep = "}\n"
        postprint = "}"
        hline =[False, False, False]
    elif preset:
        hsep_head = ["|", "|", "|"]
        hsep_line = ["+", "+", "+", "="]
        hsep = ["|", "|", "|"]
        hline =[True, True, False]
    
    def get_max():
        has_max_len = False
        for rn, r in enumerate(table):
            if not has_max_len:
                has_max_len = True
                max_len = np.zeros(len(r))
                max_at = np.zeros(len(r))
            for cn, c in enumerate(r):
                if len(str(c)) > max_len[cn]:
                       max_at[cn] = rn
                max_len[cn] = max(max_len[cn], len(str(c)))
        #print(max_len)
        #print(max_at)
        return max_len
    
    def put_hline():
        print(hsep_line[0], end="")
        for hdn, hd in enumerate(header):
            if hspace:
                try:
                    h = int(hspace[hdn])
                except:
                    h = int(max_len[hdn])
                    
            if hdn < len(header) - 1:
                try:
                    sep = hsep_line[1]
                except:
                    sep = ""
            else:
                try:
                    sep = hsep_line[2] + "\n"
                except:
                    sep = "\n"
                
            try:
                print(hsep_line[3] * h, end=sep)
            except:
                print("", end=sep)
        
    max_len = get_max()
    #print(max_len)
    if preprint:
        print(preprint, sep="")
    
    hline[0] and put_hline()
    
    # Header printing
    if header:
        print(hsep_head[0], end="")
        for hdn, hd in enumerate(header):
            if hspace:
                try:
                    h = int(hspace[hdn])
                except:
                    h = int(max_len[hdn])
                f = "{" + "0" + ":" + str(h) + "}"
                if isinstance(hd, float):
                    if precision:
                        try:
                            p = int(precision[hdn])
                        except:
                            p = min(h - 1, 7)
                    else:
                        p = min(h - 1, 7)
                    f = "{" + "0" + ":" + str(h) + "." + str(p) + "f}"
            else:
                f = "{0}"
            if hdn < len(header) - 1:
                sep = hsep_head[1]
            else:
                sep = hsep_head[2] + "\n"
            print(f.format(hd), end=sep)
        
    hline[1] and put_hline()
    
    # Table content printing
    for rn, r in enumerate(table):
        print(hsep[0], end="")
        for cn, c in enumerate(r):
            if hspace:
                try:
                    h = int(hspace[cn])
                except:
                    h = int(max_len[cn])
                f = "{" + "0" + ":" + str(h) + "}"
                if isinstance(c,float):
                    if precision:
                        try:
                            p = precision[cn]
                        except:
                            p = min(h - 3, 7)
                    else:
                        p = min(h - 3, 7)
                    f = "{" + "0" + ":" + str(h) + "." + str(p) + "f}"
            else:
                f = "{0}"
            if cn < len(r) - 1:
                sep = hsep[1]
            else:
                if rn == len(table) - 1 and not hline[2] and last_sep:
                    sep = last_sep
                else:
                    sep = hsep[2] + "\n"
            print(f.format(c), end=sep)
            
    hline[2] and put_hline()
    
    if postprint:
        print(postprint, sep="")
    
def hline():
    print("*=============================*")
    
if __name__ == "__main__":
    hdr = ["a", "b", "c"]
    data =  [
            [1,"string",3.],
            [3,"val",5.],
            [5,6,7.]
            ]
    
    hline()
    print("Default (empty) preset:")
    hline()
    print()
    
    table_print(data, header = hdr, hspace = [8])
    print()
    
    hline()
    print("LaTeX preset:")
    hline()
    print()
    
    table_print(data, header = hdr, hspace = [8], preset = "LaTeX")
    print()
    
    hline()
    print("CSV preset:")
    hline()
    print()
    
    table_print(data, header = hdr, hspace = [8], preset = "CSV")
    print()
    
    hline()
    print("Python Array preset:")
    hline()
    print()
    
    table_print(data, header = hdr, hspace = [8], preset="PyArray")
    print()
    hline()
    print("C Array preset:")
    hline()
    print()
    
    table_print(data, header = hdr, hspace = [8], preset="CArray")
    print()
    
    hline()
    print("Beautiful preset:")
    hline()
    print()
    
    table_print(data, header = hdr, hspace = [8], preset=True)
    print()
