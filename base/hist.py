#! /usr/bin/env python

import sys
import os.path
import re

import numpy as np
from ast import literal_eval

from tools import *

class Hist:
  """

  """

  def __init__(self, source):
    """
    If provided, loads a file named filename.
    """

    print("Loading istogram data")
    self.source = source
    self.filename = self.source.split("/")[-1]

    self.data = np.fromfile(self.source, dtype=np.float64)

    self.parse_muffalo()

  def parse_muffalo(self):
    self.MUFFALO = False
    try:
    #if True:
        head, tail = os.path.split(self.source)
        root, ext = os.path.splitext(tail)
        if ext != "bin":
            print("WARNING: extension doesn't look like '.bin', instead is " + ext)
        self.filename = root
        print("Trying to read MUFFALO at {}".format(head + "/" + root + ".py"))
        attrs = np.genfromtxt(head + "/" + root + ".py", delimiter='=', usecols=(0), dtype=str)
        vals = np.genfromtxt(head + "/" + root + ".py", delimiter='=', usecols=(1), dtype=str)
        for attr, val in zip(attrs, vals):
            print("Reading attr {} with value {}".format(attr, val))
            setattr(self, attr, val)

        self.name = literal_eval(self.name)
        self.dof = int(self.dof)
        self.sim_num = int(self.sim_num)
        self.coords_num = int(self.coords_num)
        self.coords = literal_eval(self.coords)
        self.indexes = literal_eval(self.indexes)
        self.component = literal_eval(self.component)
        self.dofpercomp = [ (i == "U" and 2) or
                            (i == "w" and 1) or
                            (i == "Gp" and 2)
                            for i in self.component ]
        self.cum_dofpercomp = np.hstack([[0], np.cumsum(self.dofpercomp)])
        #print(self.cum_dofpercomp)
        self.MUFFALO = True
        if self.dof*self.coords_num*self.sim_num != len(self.data):
            print("WARNING: data size seems to mismatch!!!")
    except OSError:
        print("Could not find MUFFALO-hist metadata, carrying on...")
    except AttributeError:
        print("Broken MUFFALO-hist metadata, skipping...")


  def plot(self, fig = None, axes = None, dof = None, title = None, show = False, save = False, notitle=False, clip=None, axlim=None):

    self.fig = fig

    if not self.MUFFALO:
        print("No MUFFALO, cannot interpret data, no plot!!")
        return

    if fig == None:
        import matplotlib.pyplot as plt
        fig = plt.figure()

    fig.clf()

    try:
      import matplotlib.pyplot as plt
    except ImportError:
      warning("Matplotlib was not found in your installation, cannot use this feature.")
      return
    if dof == None:
        dof = range(0, self.dof)
    plot_dof = len(dof)
    
    #a,b = rectangularize(self.dof *)
    axxarray = []
    #fig, axarray = fig.subplots(self.dof * self.coords_num, sharex=True)
    self.coords = np.array(self.coords)
    if len(self.coords.shape) == 1:
        self.coords = [self.coords]
    for coord in range(0, self.coords_num):
        for d, comp in enumerate(dof):
            i = comp + coord  * plot_dof

            for x in range(len(self.cum_dofpercomp)):
                if comp >= self.cum_dofpercomp[x] and (x < len(self.cum_dofpercomp) - 1) \
                    and comp < self.cum_dofpercomp[x+1]:
                    comp_name = self.component[x]
                    comp_num = comp - self.cum_dofpercomp[x]
                    break
            #self.f = plt.figure()
            axes = fig.add_subplot(self.coords_num, plot_dof, i+1)
            axxarray.append(axes)
            #axes = axarray[i]
            #im = axes.pcolormesh(self.meshx, self.meshy, self.data[i])


            #print(self.data[i::(self.dof*self.coords_num)])
            print(self.coords[coord])
            #print(len(self.data[i::(self.dof*self.coords_num)]))
            try:
                import pylab
            except ImportError:
                print("Module not found pylab")
            if clip:
                data = np.clip(self.data[i::(self.dof*self.coords_num)], clip[0], clip[1])
            else:
                data = self.data[i::(self.dof*self.coords_num)]
            print("Creating histogram of dof " + str(comp) + " coord " + str(coord) + "...")
            try:
                n, bins, patches = axes.hist(data, 50, normed=1, histtype='stepfilled')
            except:
                if len(np.isnan(data)) > 0:
                    print("ERROR: array contains nan's")
                else:
                    print("ERROR: UNKNOWN PLOT FAILURE!!!")


            axes.set_title(self.dofpercomp[x] > 1 and "{0}_{1} @ {2}".format(comp_name, comp_num, self.coords[coord])
                                                   or "{0} @ {1}".format(comp_name, self.coords[coord]))
                                        
            axes.title.set_fontsize(8)
            axes.xaxis.label.set_fontsize(6)
            axes.yaxis.label.set_fontsize(6)
            for tick in axes.xaxis.get_major_ticks():
                tick.label.set_fontsize(6)
                tick.label.set_rotation('vertical')
            for tick in axes.yaxis.get_major_ticks():
                tick.label.set_fontsize(6)
            #axes.get_yticklabels().set_fontsize(10)
            #axes.set_ylabel("y")
            #print(self.coords)
            #axes.axis('equal')
            #im.xlim([0,1])
            if axlim:
                try:
                    if axlim[coord][comp] != None:
                        axes.set_xlim(axlim[coord][comp])
                    else:
                        print("NO axlime!!!!!!!!!!!")
                except:
                    print("FAILED ONE axlime!!!!!!!!!!")
                    try:
                        axes.set_xlim(axlim[comp])
                    except:
                        pass
            #axes.set_ylim([0,1])
            #axes.set_aspect('equal')
            fig.patch.set_alpha(0)
            #fig.colorbar(im)
            #plt.show(block=False)

    fig.tight_layout()
    if title == None:
        fig.suptitle("{0}".format(self.name), y=1.05, fontsize=10)
    else:
        fig.suptitle(title[i], y=1.05, fontsize=10)
    if show:
        plt.show(block=False)

    if isinstance(fig, str) or save == True:
        #print(self.filename)
        name = self.filename + ".png"
        print("Saving to {0}".format(name))
        plt.savefig(name)
