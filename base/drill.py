#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Utility to get histograms from multiple data.

Description.

author: Filippo Leonardi
website: filippoleonardi.ch
last edited: January 2014
"""

import sys
sys.path.append("./")

import collections

from tools import *
import data

import numpy as np

class Drill():
    def __init__(self, coord_array):
        """
        Initialize the coordinates in the form [(x_1,y_1),...]
        """
        self.coord_array = coord_array
        self.val_array = []
        for i in range(0, len(coord_array)):
          self.val_array.append([])
          
        self.pts = len(coord_array)
      
    def push_data(self, dt):
        """
        Push one data onto the histogram.
        """
        i = 0
        for coord in self.coord_array:
          self.val_array[i].append(dt.eval(coord[0], coord[1]))
          i += 1
          
        self.last_data = dt
      
    def report(self):
        print("Obtained {0} drilled coordinates.".format(self.pts))
        print("Processed {0} data files.".format(len(self.val_array[0])))
        self.last_data.gen_info_from_name()
        print("FYI the last data info:")
        # TODO replace with own data report
        print(self.last_data.sim_name)
        print(self.last_data.size_name)
        print(self.last_data.comp_name)
        print(self.last_data.frame_name)
      
    def save(self, index = None):
        """
        Save numpy data to .npy s
        """
        
        self.last_data.gen_info_from_name()
        
        if index == None:
            index = range(0, self.pts)
        
        if index is list or isinstance(index, collections.Iterable):
            for i in index:
                self.save(i)
        else:
            beautified_tuple = "({0:.2f},{1:.2f})".format(self.coord_array[index][0], self.coord_array[index][1])
            name = "hist_{0}_{1}_{2}_{3}_{4}.npy".format(self.last_data.sim_name, self.last_data.size_name, self.last_data.comp_name, beautified_tuple, self.last_data.frame_name)
            np.save(name, self.val_array[index])
      
    def plot(self, index = None):
        """
        Plots the histogram. 
        TODO: plot as in data
        """
        
        self.last_data.gen_info_from_name()
        
        if index == None:
            index = range(0, self.pts)
        
        if index is list or isinstance(index, collections.Iterable):
            for i in index:
                pylab.clf()
                self.plot(i)
        else:
            try:
                import pylab
            except ImportError:
                print("Module not found pylab")
            n, bins, patches = pylab.hist(self.val_array[index], 50, normed=1, histtype='stepfilled')
            pylab.setp(patches, 'facecolor', 'g', 'alpha', 0.75)
            pylab.title("Histogram of {0} at {1}.".format(self.last_data.comp_name, self.coord_array[index]))
            #pylab.show()
            beautified_tuple = "({0:.2f},{1:.2f})".format(self.coord_array[index][0], self.coord_array[index][1])
            name = "hist_{0}_{1}_{2}_{3}_{4}.png".format(self.last_data.sim_name, self.last_data.size_name, self.last_data.comp_name, beautified_tuple, self.last_data.frame_name)
            pylab.savefig(name)
